#
# reader.py
#
# This is a script meant for merging debug output from the
# separate class files, as well as other possible future
# functions.
#
# Written for CS488 Fall 2013 for University of Waterloo
# by Andrew Klamut (ajklamut)
# #20281315
#

import os
import datetime
import re

OUTPUT_FILE = "out.txt"

if __name__ == "__main__":
    # Get the current directory path
    logFolder = os.path.dirname(os.path.realpath(__file__))

    # Get all the .log files in the current directory
    logFiles = [f for f in os.listdir(logFolder) if os.path.isfile(os.path.join(logFolder, f)) and f[-4:] == ".log"]

    # Process all lines from each file
    logContents = []
    stampPattern = re.compile(r'^\[([\d]+)\]')

    for logFile in logFiles:
        with open(logFile) as f:
            for line in f:
                # Each line produces a map entry
                lineEntry = {}

                # Include the timestamp
                stampMatch = stampPattern.match(line.lstrip())
                if stampMatch is None:
                    continue

                lineEntry['timestamp'] = int(stampMatch.group(1))

                # Include the content, but clean whitespace and timestamp
                lineEntry['content'] = line.lstrip().replace(stampMatch.group(0), '')
                logContents.append(lineEntry)

    # Write to merged
    depth = 0

    with open(OUTPUT_FILE, 'a') as out:
        out.write("========== File opened on " + datetime.datetime.now().strftime("%Y-%m-%d @ %H:%M:%S") + " ==========\n")

        for entry in sorted(logContents, key=lambda k: k['timestamp']):
            if entry['content'].startswith(" Exiting"):
                depth -= 1

            out.write("  " * depth + entry['content'])

            if entry['content'].startswith(" Entering"):
                depth += 1
