/*
 * leg.cpp
 *
 * TODO
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#include "leg.hpp"
#include "debugstream.hpp"
#include "realnode.hpp"
#include "sphere.hpp"
#include <cmath>
#include <iostream>

Leg::Leg(std::string desc) : VirtualNode(desc), dout(DebugManager::getInstance().newStream("Leg")) {
    dout.enterScope("Leg::Leg");

    std::string namePrefix = "";
    if (m_desc != "") {
        namePrefix = m_desc + "|";
    }

    // Create the thigh real leaf relative leg's origin
    RealNode *thigh = new RealNode(new Sphere(), namePrefix + "thigh");
    thigh->transform(ModelNode::translateMatrix(Vector4D(0, -3.5, 0, 0)));
    thigh->transform(ModelNode::scaleMatrix(Vector4D(1.05, 3.5, 1, 0)));
    // Add as child to the main leg
    addChild(thigh);

    // Create the knee virtual child relative leg's origin
    m_knee = new VirtualNode(namePrefix + "knee");
    m_knee->transform(ModelNode::translateMatrix(Vector4D(0, -6.5, 0, 0)));
    // Add as child to the main leg
    addChild(m_knee);

    // Create the calf real leaf relative knee's origin
    RealNode *calf = new RealNode(new Sphere(), namePrefix + "calf");
    calf->transform(ModelNode::translateMatrix(Vector4D(0, -3, 0, 0)));
    calf->transform(ModelNode::scaleMatrix(Vector4D(1.05, 2.9, 0.8, 0)));
    // Add as child to the knee
    m_knee->addChild(calf);

    // Create the ankle virtual child relative knee's origin
    m_ankle = new VirtualNode(namePrefix + "ankle");
    m_ankle->transform(ModelNode::translateMatrix(Vector4D(0, -6, 0, 0)));
    // Add as child to the knee
    m_knee->addChild(m_ankle);

    // Create the foot real leaf relative ankle's origin
    RealNode *foot = new RealNode(new Sphere(), namePrefix + "foot");
    foot->transform(ModelNode::translateMatrix(Vector4D(-2, 0, 0, 0)));
    foot->transform(ModelNode::scaleMatrix(Vector4D(2.05, 0.65, 0.75, 0)));
    m_ankle->addChild(foot);

    // Initialize members
    m_thighFlexFactor = m_calfFlexFactor = m_footFlexFactor = 0;

    dout.exitScope();
}

Leg::~Leg() {
    dout.enterScope("Leg::~Leg");

    dout.exitScope();
}

bool Leg::flexThigh(double factor) {
    dout.enterScope("Leg::flexThigh");

    if (m_thighFlexFactor + factor > M_PI / 2) {
        transform(ModelNode::rotateMatrix('z', M_PI / 2 - m_thighFlexFactor));
        m_thighFlexFactor = M_PI / 2;
        dout.exitScope();
        return false;
    } else if (m_thighFlexFactor + factor < M_PI / -2) {
        transform(ModelNode::rotateMatrix('z', M_PI / -2 - m_thighFlexFactor));
        m_thighFlexFactor = M_PI / -2;
        dout.exitScope();
        return false;
    } else {
        transform(ModelNode::rotateMatrix('z', factor));
        m_thighFlexFactor += factor;
        dout.exitScope();
        return true;
    }
}

bool Leg::flexCalf(double factor) {
    dout.enterScope("Leg::flexCalf");

    if (m_calfFlexFactor + factor > (7 * M_PI / 8)) {
        m_knee->transform(ModelNode::rotateMatrix('z', (7 * M_PI / 8) - m_calfFlexFactor));
        m_calfFlexFactor = (7 * M_PI / 8);
        dout.exitScope();
        return false;
    } else if (m_calfFlexFactor + factor < 0) {
        m_knee->transform(ModelNode::rotateMatrix('z', 0 - m_calfFlexFactor));
        m_calfFlexFactor = 0;
        dout.exitScope();
        return false;
    } else {
        m_knee->transform(ModelNode::rotateMatrix('z', factor));
        m_calfFlexFactor += factor;
        dout.exitScope();
        return true;
    }
}

bool Leg::flexFoot(double factor) {
    dout.enterScope("Leg::flexFoot");

    if (m_footFlexFactor + factor > (5 * M_PI / 12)) {
        m_ankle->transform(ModelNode::rotateMatrix('z', (5 * M_PI / 12) - m_footFlexFactor));
        m_footFlexFactor = (5 * M_PI / 12);
        dout.exitScope();
        return false;
    } else if (m_footFlexFactor + factor < M_PI / -6) {
        m_ankle->transform(ModelNode::rotateMatrix('z', (M_PI / -6) - m_footFlexFactor));
        m_footFlexFactor = M_PI / -6;
        dout.exitScope();
        return false;
    } else {
        m_ankle->transform(ModelNode::rotateMatrix('z', factor));
        m_footFlexFactor += factor;
        dout.exitScope();
        return true;
    }
}

void Leg::printTransform() const {
    m_ankle->printTransform();
}
