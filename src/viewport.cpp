/*
 * viewport.cpp
 *
 * This object should not have much complexity beyond resizing
 * logic. Its main purpose is to traverse the model tree and
 * call their draw() methods.
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#include "viewport.hpp"
#include "gamesettings.hpp"
#include "debugstream.hpp"
#include "modelnode.hpp"
#include "realnode.hpp"
#include <GL/glu.h>
#include <iostream>

ViewportException::ViewportException(std::string msg) : std::runtime_error(msg) {

}

Viewport::Viewport() : dout(DebugManager::getInstance().newStream("Viewport")) {
    dout.enterScope("Viewport::Viewport");

    Glib::RefPtr<Gdk::GL::Config> glconfig;
    GameSettings &s = GameSettings::getInstance();

    // Ask for an OpenGL Setup with
    //  - red, green and blue component colour
    //  - a depth buffer to avoid things overlapping wrongly
    //  - double-buffered rendering to avoid tearing/flickering
    glconfig = Gdk::GL::Config::create(Gdk::GL::MODE_RGB | Gdk::GL::MODE_DEPTH | Gdk::GL::MODE_DOUBLE);

    if (glconfig == 0) {
        // If we can't get this configuration, die
        std::cerr << "Unable to setup OpenGL Configuration!" << std::endl;
        abort();
    }

    // Accept the configuration
    set_gl_capability(glconfig);

    // Register for events from mouse, keyboard, and the OS
    add_events(Gdk::BUTTON1_MOTION_MASK    |
               Gdk::BUTTON2_MOTION_MASK    |
               Gdk::BUTTON3_MOTION_MASK    |
               Gdk::BUTTON_PRESS_MASK      | 
               Gdk::BUTTON_RELEASE_MASK    |
               Gdk::KEY_PRESS_MASK         |
               Gdk::SCROLL_MASK            |
               Gdk::VISIBILITY_NOTIFY_MASK);

    // Set members
    m_modelRoot = NULL;
    m_pickRequest = NULL;
    m_pickBuffer = new GLuint[s[GameSettings::PICK_BUFFER_SIZE]];
    m_wireframe = m_backcull = m_frontcull = false;

    // Set the size of the screen
    set_size_request(s[GameSettings::VIEWPORT_WIDTH], s[GameSettings::VIEWPORT_HEIGHT]);

    // Setup a timer to call invalidate() every GameDefaults::INVALIDATE_TIMEOUT_MS milliseconds
    Glib::signal_timeout().connect(sigc::mem_fun(*this, &Viewport::invalidate), s[GameSettings::INVALIDATE_TIMEOUT_MS]);

    dout.exitScope();
}

Viewport::~Viewport() {
    dout.enterScope("Viewport::~Viewport");

    // Delete the pick buffer
    delete m_pickBuffer;
    m_pickBuffer = NULL;

    // Delete outstanding pick request if it exists
    if (m_pickRequest != NULL) {
        delete m_pickRequest;
        m_pickRequest = NULL;
    }

    dout.exitScope();
}

sigc::signal<void, Viewport::PickResponse> Viewport::pickedSignal() const {
    dout.enterScope("Viewport::pickedSignal");

    dout.exitScope();
    return sig_modelPicked;
}

bool Viewport::invalidate() {
    //dout.enterScope("Viewport::invalidate");

    // Force a rerender
    Gtk::Allocation allocation = get_allocation();
    get_window()->invalidate_rect(allocation, false);

    //dout.exitScope();
    return true;
}

void Viewport::attachModel(ModelNode *root) {
    dout.enterScope("Viewport::attachModel");
    m_modelRoot = root;
    dout.exitScope();
}

void Viewport::toggleWireFrame() {
    dout.enterScope("Viewport::toggleWireFrame");

    m_wireframe = !m_wireframe;

    if (m_wireframe) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    } else {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }

    dout.exitScope();
}

void Viewport::toggleBackCull() {
    dout.enterScope("Viewport::toggleBackCull");

    m_backcull = !m_backcull;

    if (m_backcull) {
        if (m_frontcull) {
            glCullFace(GL_FRONT_AND_BACK);
        } else {
            glEnable(GL_CULL_FACE);
            glCullFace(GL_BACK);
        }
    } else {
        if (m_frontcull) {
            glCullFace(GL_FRONT);
        } else {
            glDisable(GL_CULL_FACE);
        }
    }

    dout.exitScope();
}

void Viewport::toggleFrontCull() {
    dout.enterScope("Viewport::toggleFrontCull");

    m_frontcull = !m_frontcull;

    if (m_frontcull) {
        if (m_backcull) {
            glCullFace(GL_FRONT_AND_BACK);
        } else {
            glEnable(GL_CULL_FACE);
            glCullFace(GL_FRONT);
        }
    } else {
        if (m_backcull) {
            glCullFace(GL_BACK);
        } else {
            glDisable(GL_CULL_FACE);
        }
    }

    dout.exitScope();
}

void Viewport::pick(int x, int y, unsigned w, unsigned h) {
    dout.enterScope("Viewport::pick");

    if (m_pickRequest != NULL) {
        // TODO: make this a queue
        dout << "Dropped pick request :(" << std::endl;
        return;
    }

    // Create the pick request member based on args
    m_pickRequest = new Viewport::PickRequest();
    m_pickRequest->x = x;
    m_pickRequest->y = get_height() - y;
    m_pickRequest->w = w;
    m_pickRequest->h = h;

    // Clear the modelnode picking names
    RealNode::resetPickingNames();

    // Manual invalidate, which triggers expose event, which will pick up the request member
    invalidate();

    dout.exitScope();
}

void Viewport::on_realize() {
    dout.enterScope("Viewport::on_realize");

    // OpenGL setup.
    // Base method
    Gtk::GL::DrawingArea::on_realize();

    // Set aspect ratio
    m_aspect = (GLfloat)get_width()/(GLfloat)get_height();

    Glib::RefPtr<Gdk::GL::Drawable> gldrawable = get_gl_drawable();

    if (!gldrawable) {
        return;
    }

    if (!gldrawable->gl_begin(get_gl_context())) {
        return;
    }

    glShadeModel(GL_SMOOTH);
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    GameSettings &s = GameSettings::getInstance();
    glSelectBuffer(s[GameSettings::PICK_BUFFER_SIZE], m_pickBuffer);

    gldrawable->gl_end();

    dout.exitScope();
}

bool Viewport::on_expose_event(GdkEventExpose* event) {
    //dout.enterScope("Viewport::on_expose_event");

    Glib::RefPtr<Gdk::GL::Drawable> gldrawable = get_gl_drawable();
    GLint viewport[4];

    if (!gldrawable) {
        return false;
    }

    if (!gldrawable->gl_begin(get_gl_context())) {
        return false;
    }

    glViewport(0, 0, get_width(), get_height());

    // Change render mode if we have an outstanding pick request
    if (m_pickRequest != NULL) {
        glRenderMode(GL_SELECT);
        glInitNames();
        glPushName(0);
    }

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();

    // Set pick matrix if we have an outstanding pick request
    if (m_pickRequest != NULL) {
        glGetIntegerv(GL_VIEWPORT, viewport);
        gluPickMatrix(m_pickRequest->x, m_pickRequest->y, m_pickRequest->w, m_pickRequest->h, viewport);
    }

    // Set up for perspective drawing 
    gluPerspective(40.0, m_aspect, 0.1, 1000.0);

    // Change to model view for drawing
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // Clear framebuffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Set up lighting

    // Apply view transforms
    // Push camera away from the origin
    glTranslated(0.0, 0.0, -40.0);

    // Draw stuff
    if (m_modelRoot != NULL) {
        if (m_pickRequest != NULL) {
            m_modelRoot->walk(ModelNode::PICK);
        } else if (m_wireframe) {
            m_modelRoot->walk(ModelNode::DRAW_DEBUG);
        } else {
            m_modelRoot->walk(ModelNode::DRAW);
        }
    }

    // Complete pick request if there was one
    if (m_pickRequest != NULL) {
        glMatrixMode(GL_PROJECTION);
        glPopMatrix();
        glFlush();

        processHits(glRenderMode(GL_RENDER));
        delete m_pickRequest;
        m_pickRequest = NULL;
    }

    // Swap the contents of the front and back buffers so we see what we
    // just drew. This should only be done if double buffering is enabled.
    gldrawable->swap_buffers();

    gldrawable->gl_end();

    //dout.exitScope();
    return true;
}

bool Viewport::on_configure_event(GdkEventConfigure* event) {
    dout.enterScope("Viewport::on_configure_event");

    Glib::RefPtr<Gdk::GL::Drawable> gldrawable = get_gl_drawable();

    if (!gldrawable) {
        return false;
    }

    if (!gldrawable->gl_begin(get_gl_context())) {
        return false;
    }

    // Set up perspective projection, using current size and aspect
    // ratio of display
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, event->width, event->height);
    gluPerspective(40.0, (GLfloat)event->width/(GLfloat)event->height, 0.1, 1000.0);

    // Update aspect member
    m_aspect = (GLfloat)event->width/(GLfloat)event->height;

    // Reset to modelview matrix mode
    glMatrixMode(GL_MODELVIEW);

    gldrawable->gl_end();

    dout.exitScope();
    return true;
}

void Viewport::processHits(GLuint hits) {
    dout.enterScope("Viewport::processHits");

    GLuint *cursor = m_pickBuffer, names, name;
    PickResponse pr;
    dout << "Got " << hits << " hits" << std::endl;

    if (hits > 0) {
        // For each hit
        for (unsigned i = 0; i < hits; i++) {
            names = *cursor++;

            dout << names << " names were included" << std::endl;
            dout << "zmin is " << *cursor++ << std::endl;
            dout << "zmax is " << *cursor++ << std::endl;

            // For each model touched by this hit
            for (unsigned j = 0; j < names; j++) {
                name = *cursor++;
                dout << "Name: " << name << std::endl;

                // Skip name = 0 since this indicates the bottom of our name stack and does not represent anything
                if (name != 0) {
                    // This is kind of nice because it reverses the weird deepest-to-shallowest GL ordering
                    pr.models.push_front(RealNode::getByPickingName(name));
                }
            }
        }
    }

    // Send out all picks and hits as a signal
    sig_modelPicked.emit(pr);

    pr.models.clear();

    dout.exitScope();
}
