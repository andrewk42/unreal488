/*
 * debugstream.hpp
 *
 * This is an object used for debug output. It acts as a stream
 * to a file specific to a name (ideally the class name). It
 * provides an indent/outdent stack for more readable output.
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#ifndef DEBUGSTREAM_H
#define DEBUGSTREAM_H

#include <sstream>
#include <fstream>
#include <stack>
#include <stdexcept>
#include <map>

class DebugException : public std::runtime_error {
  public:
    DebugException(std::string msg = "DebugException");
};

class DebugStream : public std::ostream {
  public:
    DebugStream(std::string className);

    // Use these at the beginning/end of methods to indent output
    void enterScope(std::string scopeName);
    void exitScope();

  private:
    class DebugBuf : public std::stringbuf {
      public:
        DebugBuf(std::string fileName);
        virtual ~DebugBuf();

        virtual int sync();

        std::ofstream m_outFile;
        std::stack<std::string> m_scope;
    };

    DebugBuf m_buffer;
};

class DebugManager {
  public:
    static DebugManager& getInstance();
    virtual ~DebugManager();

    DebugStream& newStream(std::string name);
    unsigned nextSequence();

  private:
    // Methods
    DebugManager();
    DebugManager(const DebugManager& other);
    void operator=(const DebugManager& other);

    // Members
    std::map<std::string, DebugStream*> m_streams;
    unsigned m_sequence;
};

#endif // DEBUGSTREAM
