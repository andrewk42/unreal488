/*
 * virtualnode.hpp
 *
 * A type of ModelNode that represents a virtual object.
 * Its key property is not having a graphical representation,
 * because it is used for nonvisual purposes (e.g. the joints
 * between the character's limbs)
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#ifndef VIRTUALNODE_HPP
#define VIRTUALNODE_HPP

#include "modelnode.hpp"

class VirtualNode : public ModelNode {
  public:
    explicit VirtualNode(std::string desc = "");
    ~VirtualNode();

    virtual void draw() const;

    virtual std::string print(bool root = true) const;

  protected:
    // Methods
    virtual void walkPush(WalkMode mode);
    virtual void walkBody(WalkMode mode);
    virtual void walkPop(WalkMode mode);

  private:
    // Members
    // Debug output
    DebugStream& dout;
};

std::ostream& operator<<(std::ostream& os, const VirtualNode& n);

#endif // VIRTUALNODE_HPP
