/*
 * trackball.cpp
 *
 * TODO
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#include "trackball.hpp"
#include "debugstream.hpp"
#include "viewport.hpp"
#include <cmath>

Trackball::Trackball() : dout(DebugManager::getInstance().newStream("Trackball")) {
    dout.enterScope("Trackball::Trackball");
    m_radius = 1;
    dout.exitScope();
}

Trackball::~Trackball() {

}

Matrix4x4 Trackball::getTransform(unsigned newX, unsigned newY, unsigned oldX, unsigned oldY) const {
    dout.enterScope("Trackball::getTransform");

    // First, express x/y values in trackball space (relative to the centre of the vtrackball)
    // x values are relative to the left side of the screen, so subtract half the width to make relative to centre
    double newRelX = (double)newX - (double)m_viewportWidth / 2;
    double oldRelX = (double)oldX - (double)m_viewportWidth / 2;
    // y values are relative to the top of the screen, so subtract that from half the height to make relative to centre
    double newRelY = (double)m_viewportHeight / 2 - (double)newY;
    double oldRelY = (double)m_viewportHeight / 2 - (double)oldY;

    // Get vectors pointing from centre of vtrackball to each mouse position
    Vector4D newRel = getVector(newRelX, newRelY);
    Vector4D oldRel = getVector(oldRelX, oldRelY);

    // Generate rotation vector by calculating cross product
    Vector4D rotation = oldRel.cross(newRel);

    // Find the angle of rotation, which is the length of the vector
    double radians = rotation.length();

    // Return the identity matrix if length is 0
    if (radians <= 0) {
        return Matrix4x4();
    }

    // Normalize the rotation vector and use to construct final transform matrix
    Vector4D normRotation = rotation.normalize();
    Matrix4x4 ret;

    ret[0][0] = cos(radians) + normRotation[0] * normRotation[0] * (1 - cos(radians));
    ret[0][1] = normRotation[0] * normRotation[1] * (1 - cos(radians)) + normRotation[2] * sin(radians);
    ret[0][2] = normRotation[0] * normRotation[2] * (1 - cos(radians)) - normRotation[1] * sin(radians);
    ret[0][3] = 0;

    ret[1][0] = normRotation[0] * normRotation[1] * (1 - cos(radians)) - sin(radians) * normRotation[2];
    ret[1][1] = cos(radians) + normRotation[1] * normRotation[1] * (1 - cos(radians));
    ret[1][2] = normRotation[1] * normRotation[2] * (1 - cos(radians)) + sin(radians) * normRotation[0];
    ret[1][3] = 0;

    ret[2][0] = normRotation[2] * normRotation[0] * (1 - cos(radians)) + sin(radians) * normRotation[1];
    ret[2][1] = normRotation[2] * normRotation[1] * (1 - cos(radians)) - sin(radians) * normRotation[0];
    ret[2][2] = cos(radians) + normRotation[2] * normRotation[2] * (1 - cos(radians));
    ret[2][3] = 0;

    ret[3][0] = 0;
    ret[3][1] = 0;
    ret[3][2] = 0;
    ret[3][3] = 1;

    dout.exitScope();
    return ret;
}

bool Trackball::viewportResized(GdkEventConfigure* event) {
    dout.enterScope("Trackball::viewportResized");

    m_viewportWidth = event->width;
    m_viewportHeight = event->height;

    // Use 33% of the larger dimension for the trackball radius
    if (event->width >= event->height) {
        m_radius = (double)event->width / 3;
    } else {
        m_radius = (double)event->height / 3;
    }
    dout << "Set radius to " << m_radius << std::endl;

    dout.exitScope();
    return false;
}

Vector4D Trackball::getVector(double x, double y) const {
    dout.enterScope("Trackball::getVector");

    // Regardless of anything, normalize x y and z
    // x and y are easy because they lie in the same plane as the arguments
    // z does not lie in the same plane and is calculated by solving for it
    // given the equation of a unit sphere 1 = x^2 + y^2 + z^2
    // (we already normalized x and y so we can assume radius of 1 for this part)
    double relX = x, relY = y, relZ;
    relX /= m_radius;
    relY /= m_radius;
    relZ = 1 - relX * relX - relY * relY;

    // At this point relZ is one step away from normalization (still need relZ = sqrt(relZ))

    // If the z component is negative, the point is outside of the virtual trackball
    if (relZ < 0) {
        // Don't want imaginary numbers, so first find the extra length past the radius
        double extraLength = sqrt(1 - relZ);

        // Further normalize x and y to match new length
        relX /= extraLength;
        relY /= extraLength;

        // Set z to 0
        relZ = 0;

    } else {
        // If the z component is positive, we complete normalization normally
        relZ = sqrt(relZ);
    }

    dout.exitScope();
    return Vector4D(relX, relY, relZ, 0);
}
