/*
 * main.cpp
 *
 * This is the main entry point for the game. It initializes
 * the GameSettings instance, creates the GameWindow, and
 * passes control to it
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#include "gamesettings.hpp"
#include "debugstream.hpp"
#include "gamewindow.hpp"
#include <gtkglmm.h>
#include <iostream>

using namespace std;

int main(int argc, char** argv) {
    // Initialize gtkmm window manager
    Gtk::Main kit(argc, argv);

    // Initialize OpenGL
    Gtk::GL::init(argc, argv);

    // Initialize GameSettings by referencing it (must do this before anything else)
    GameSettings::getInstance();

    DebugStream dout("main");

    dout << "Welcome to Unreal488!" << endl;

    // Create GameWindow
    GameWindow w;

    // Run GameWindow
    Gtk::Main::run(w);
}
