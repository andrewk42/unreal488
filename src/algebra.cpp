/*
 * algebra.cpp
 *
 * TODO
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#include "algebra.hpp"
#include "debugstream.hpp"
#include <cmath>


/*
 * AlgebraException Definitions
 */

AlgebraException::AlgebraException(std::string msg) : std::runtime_error(msg) {

}


/*
 * Vector4D Definitions
 */

Vector4D::Vector4D(double x, double y, double z, double w) : dout(DebugManager::getInstance().newStream("Vector4D")) {
    //dout.enterScope("Vector4D::Vector4D");

    m_data[0] = x;
    m_data[1] = y;
    m_data[2] = z;
    m_data[3] = w;

    //dout.exitScope();
}

Vector4D::Vector4D(const Vector4D& other) : dout(other.dout) {
    //dout.enterScope("Vector4D::Vector4D");

    m_data[0] = other.m_data[0];
    m_data[1] = other.m_data[1];
    m_data[2] = other.m_data[2];
    m_data[3] = other.m_data[3];

    //dout.exitScope();
}

Vector4D& Vector4D::operator=(const Vector4D& other) {
    dout.enterScope("Vector4D::operator=");

    m_data[0] = other.m_data[0];
    m_data[1] = other.m_data[1];
    m_data[2] = other.m_data[2];
    m_data[3] = other.m_data[3];

    dout.exitScope();
    return *this;
}

double& Vector4D::operator[](unsigned index) {
    return m_data[index];
}

double Vector4D::operator[](unsigned index) const {
    return m_data[index];
}

Vector4D Vector4D::cross(const Vector4D& other) const {
    return Vector4D(
            m_data[1] * other[2] - m_data[2] * other[1],
            m_data[2] * other[0] - m_data[0] * other[2],
            m_data[0] * other[1] - m_data[1] * other[0]);
}

double Vector4D::length() const {
    return sqrt(m_data[0] * m_data[0] + m_data[1] * m_data[1] + m_data[2] * m_data[2]);
}

Vector4D Vector4D::normalize() const {
    return Vector4D(m_data[0] / length(), m_data[1] / length(), m_data[2] / length(), m_data[3]);
}

std::ostream& operator<<(std::ostream& os, const Vector4D& v) {
    return os << "<" << v[0] << "," << v[1] << "," << v[2] << "," << v[3] << ">" << std::endl;
}


/*
 * Matrix4x4 Definitions
 */

Matrix4x4::Matrix4x4() : dout(DebugManager::getInstance().newStream("Matrix4x4")) {
    //dout.enterScope("Matrix4x4::Matrix4x4");

    // With no arguments just do identity matrix
    std::fill(m_data, m_data + 16, 0);
    m_data[0] = 1;
    m_data[5] = 1;
    m_data[10] = 1;
    m_data[15] = 1;

    //dout.exitScope();
}

Matrix4x4::Matrix4x4(const Vector4D row1, const Vector4D row2, const Vector4D row3, const Vector4D row4) : dout(DebugManager::getInstance().newStream("Matrix4x4")) {
    //dout.enterScope("Matrix4x4::Matrix4x4");

    m_data[0] = row1[0];
    m_data[1] = row1[1];
    m_data[2] = row1[2];
    m_data[3] = row1[3];

    m_data[4] = row2[0];
    m_data[5] = row2[1];
    m_data[6] = row2[2];
    m_data[7] = row2[3];

    m_data[8] = row3[0];
    m_data[9] = row3[1];
    m_data[10] = row3[2];
    m_data[11] = row3[3];

    m_data[12] = row4[0];
    m_data[13] = row4[1];
    m_data[14] = row4[2];
    m_data[15] = row4[3];

    //dout.exitScope();
}

Matrix4x4::Matrix4x4(const Matrix4x4& other) : dout(other.dout) {
    //dout.enterScope("Matrix4x4::Matrix4x4");

    std::copy(other.m_data, other.m_data + 16, m_data);

    //dout.exitScope();
}

Matrix4x4& Matrix4x4::operator=(const Matrix4x4& other) {
    dout.enterScope("Matrix4x4::operator=");

    std::copy(other.m_data, other.m_data + 16, m_data);

    dout.exitScope();
    return *this;
}

Vector4D Matrix4x4::getRow(unsigned row) const {
    return Vector4D(m_data[row * 4], m_data[row * 4 + 1], m_data[row * 4 + 2], m_data[row * 4 + 3]);
}

double * Matrix4x4::getRow(unsigned row) {
    return (double *)m_data + row * 4;
}

Vector4D Matrix4x4::getColumn(unsigned col) const {
    return Vector4D(m_data[col], m_data[col + 4], m_data[col + 8], m_data[col + 12]);
}

Vector4D Matrix4x4::operator[](unsigned row) const {
    return getRow(row);
}

double * Matrix4x4::operator[](unsigned row) {
    return getRow(row);
}

Matrix4x4 Matrix4x4::transpose() const {
    return Matrix4x4(getColumn(0), getColumn(1), getColumn(2), getColumn(3));
}

/*
 * Some helper functions for Gauss-Jordan elimination
 */

static void swaprows(Matrix4x4& a, unsigned r1, unsigned r2) {
    std::swap(a[r1][0], a[r2][0]);
    std::swap(a[r1][1], a[r2][1]);
    std::swap(a[r1][2], a[r2][2]);
    std::swap(a[r1][3], a[r2][3]);
}

static void dividerow(Matrix4x4& a, unsigned r, double fac) {
    a[r][0] /= fac;
    a[r][1] /= fac;
    a[r][2] /= fac;
    a[r][3] /= fac;
}

static void submultrow(Matrix4x4& a, unsigned dest, unsigned src, double fac) {
    a[dest][0] -= fac * a[src][0];
    a[dest][1] -= fac * a[src][1];
    a[dest][2] -= fac * a[src][2];
    a[dest][3] -= fac * a[src][3];
}

Matrix4x4 Matrix4x4::invert() const {
    dout.enterScope("Matrix4x4::invert");

    // The original matrix, which we will perform ops on to get identity matrix
    Matrix4x4 a(*this);
    // The target matrix, which starts as identity and becomes inv(a) through same ops
    Matrix4x4 ret;

    // Loop over each column
    for (unsigned j = 0; j < 4; j++) {
        // Start by guessing the same numbered row has the largest element in this column
        unsigned pivot = j;

        // Check the rest of the rows below the jth one (don't need above because diagonal)
        for (unsigned i = j + 1; i < 4; i++) {
            if (fabs(a[i][j]) > fabs(a[pivot][j])) {
                pivot = i;
            }
        }

        // Swap rows pivot and j in a and ret to put pivot on diagonal
        swaprows(a, pivot, j);
        swaprows(ret, pivot, j);

        // Fail if the pivot is 0
        if (a[j][j] == 0) {
            throw AlgebraException("Matrix is not invertible!");
        }

        // Scale row j to have a unit diagonal
        dividerow(ret, j, a[j][j]);
        dividerow(a, j, a[j][j]);

        // Eliminate non-diagonal elems
        for (unsigned i = 0; i < 4; i++) {
            if (i != j) {
                submultrow(ret, i, j, a[i][j]);
                submultrow(a, i, j, a[i][j]);
            }
        }
    }

    dout.exitScope();
    return ret;
}

const double * Matrix4x4::begin() const {
    return (double *)m_data;
}

const double * Matrix4x4::end() const {
    return begin() + 16;
}

Vector4D Matrix4x4::operator*(const Vector4D& v) const {
    return Vector4D(
            v[0] * (*this)[0][0] + v[1] * (*this)[0][1] + v[2] * (*this)[0][2] + v[3] * (*this)[0][3],
            v[0] * (*this)[1][0] + v[1] * (*this)[1][1] + v[2] * (*this)[1][2] + v[3] * (*this)[1][3],
            v[0] * (*this)[2][0] + v[1] * (*this)[2][1] + v[2] * (*this)[2][2] + v[3] * (*this)[2][3],
            v[0] * (*this)[3][0] + v[1] * (*this)[3][1] + v[2] * (*this)[3][2] + v[3] * (*this)[3][3]);
}

void Matrix4x4::operator*=(const Matrix4x4& other) {
    Matrix4x4 res = *this * other;

    for (unsigned i = 0; i < 16; i++) {
        m_data[i] = res.m_data[i];
    }
}

Matrix4x4 operator*(const Matrix4x4& a, const Matrix4x4& b) {
    Matrix4x4 ret;

    for (unsigned i = 0; i < 4; i++) {
        Vector4D row = a.getRow(i);

        for (unsigned j = 0; j < 4; j++) {
            ret[i][j] = row[0] * b[0][j] + row[1] * b[1][j] + row[2] * b[2][j] + row[3] * b[3][j];
        }
    }

    return ret;
}

std::ostream& operator<<(std::ostream& os, const Matrix4x4& m) {
    return os << "[" << m[0][0] << " " << m[0][1] << " " 
            << m[0][2] << " " << m[0][3] << "]" << std::endl
            << "[" << m[1][0] << " " << m[1][1] << " " 
            << m[1][2] << " " << m[1][3] << "]" << std::endl
            << "[" << m[2][0] << " " << m[2][1] << " " 
            << m[2][2] << " " << m[2][3] << "]" << std::endl
            << "[" << m[3][0] << " " << m[3][1] << " " 
            << m[3][2] << " " << m[3][3] << "]";
}
