/*
 * gamewindow.hpp
 *
 * This is the game window. It contains the menus and the graphic
 * viewport, which together serve as a frontend to the user.
 *
 * Programmatically it also contains all the top-level application
 * objects and handles memory management.
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#ifndef GAMEWINDOW_HPP
#define GAMEWINDOW_HPP

#include "viewport.hpp"
#include "engine.hpp"
#include "trackball.hpp"
#include <gtkmm.h>

class ModelNode;
class DebugStream;

class GameWindow : public Gtk::Window {
  public:
    GameWindow();
    virtual ~GameWindow();

  protected:
    // Methods
    // Slots
    virtual bool onButtonPress(GdkEventButton* event);
    virtual bool onButtonRelease(GdkEventButton* event);
    virtual bool onButtonMotion(GdkEventMotion* event);
    virtual bool on_key_press_event(GdkEventKey* event);
    virtual bool on_key_release_event(GdkEventKey* event);
    virtual bool onMouseScroll(GdkEventScroll* event);

    // Members
    // The status of mouse buttons
    bool m_leftPressed, m_rightPressed, m_middlePressed;
    // The status of mouse motion
    bool m_motionLatch;
    int m_lastX, m_lastY;
    // The status of keyboard keys
    bool m_shiftPressed, m_tPressed, m_sPressed, m_rPressed;

  private:
    // Methods
    void initializeMenus();
    void initializeDesignMenus();
    void changeGame(Engine::GameType t);
    Engine::GameInput getCurrentInput() const;

    // Members
    // Main viewport container, vertical orientation
    Gtk::VBox m_vbox;
    // Main menubar
    Gtk::MenuBar m_menuBar;
    // Submenus
    Gtk::Menu m_appMenu, m_optionMenu;
    // The viewport
    Viewport m_viewport;
    // The game engine
    Engine m_engine;
    // The model tree
    ModelNode* m_modelRoot;
    // The virtual trackball
    Trackball m_trackball;
    // Debug output
    DebugStream& dout;
};

#endif // GAMEWINDOW_HPP
