/*
 * leg.hpp
 *
 * 
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#ifndef LEG_HPP
#define LEG_HPP

#include "virtualnode.hpp"

class Leg : public VirtualNode {
  public:
    explicit Leg(std::string desc = "");
    virtual ~Leg();

    bool flexThigh(double factor);
    bool flexCalf(double factor);
    bool flexFoot(double factor);
    virtual void printTransform() const;

  protected:
    VirtualNode *m_knee, *m_ankle;
    double m_thighFlexFactor, m_calfFlexFactor, m_footFlexFactor;

  private:
    DebugStream& dout;
};

#endif // LEG_HPP
