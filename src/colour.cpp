/*
 * colour.cpp
 *
 * TODO
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#include "colour.hpp"
#include "debugstream.hpp"

Colour::Colour(Colour::Preset p) : dout(DebugManager::getInstance().newStream("Colour")) {
    dout.enterScope("Colour::Colour");

    switch (p) {
      case BLACK:
        m_red = 0;
        m_green = 0;
        m_blue = 0;
        break;
      case RED:
        m_red = 1;
        m_green = 0;
        m_blue = 0;
        break;
      case GREEN:
        m_red = 0;
        m_green = 1;
        m_blue = 0;
        break;
      case BLUE:
        m_red = 0;
        m_green = 0;
        m_blue = 1;
        break;
      case YELLOW:
        m_red = 1;
        m_green = 1;
        m_blue = 0;
        break;
      case MAGENTA:
        m_red = 1;
        m_green = 0;
        m_blue = 1;
        break;
      case CYAN:
        m_red = 0;
        m_green = 1;
        m_blue = 1;
        break;
      case WHITE:
        m_red = 1;
        m_green = 1;
        m_blue = 1;
        break;
    }

    dout.exitScope();
}

Colour::Colour(double r, double g, double b) : dout(DebugManager::getInstance().newStream("Colour")) {
    dout.enterScope("Colour::Colour");

    dout << "Got arguments " << r << ", " << g << ", " << b << std::endl;
    m_red = r;
    m_green = g;
    m_blue = b;

    dout.exitScope();
}

Colour::Colour(const Colour& other) : dout(other.dout) {
    dout.enterScope("Colour::Colour");

    m_red = other.m_red;
    m_green = other.m_green;
    m_blue = other.m_blue;

    dout.exitScope();
}

Colour& Colour::operator=(const Colour& other) {
    dout.enterScope("Colour::operator=");

    m_red = other.m_red;
    m_green = other.m_green;
    m_blue = other.m_blue;

    dout.exitScope();
    return *this;
}

double Colour::R() const {
    return m_red;
}

double Colour::G() const {
    return m_green;
}

double Colour::B() const {
    return m_blue;
}

Colour Colour::operator*(double s) const {
    return Colour(R() * s, G() * s, B() * s);
}

Colour operator*(double s, const Colour& c) {
    return Colour(s * c.R(), s * c.G(), s * c.B());
}

Colour operator*(const Colour& c1, const Colour& c2) {
    return Colour(c1.R() * c2.R(), c1.G() * c2.G(), c1.B() * c2.B());
}

Colour operator+(const Colour& c1, const Colour& c2) {
    return Colour(c1.R() + c2.R(), c1.G() + c2.G(), c1.B() + c2.B());
}

bool operator==(const Colour& c1, const Colour& c2) {
    return (c1.R() == c2.R() && c1.G() == c2.G() && c1.B() == c2.B());
}

std::ostream& operator<<(std::ostream& os, const Colour& c) {
    return os << "Colour(" << c.R() << ", " << c.G() << ", " << c.B();
}
