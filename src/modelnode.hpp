/*
 * modelnode.hpp
 *
 * This is the outermost model object. It is arranged in a
 * tree structure, where each node contains a list of pointers
 * to children nodes.
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#ifndef MODELNODE_HPP
#define MODELNODE_HPP

#include "algebra.hpp"
#include <list>
#include <ostream>

class DebugStream;

class ModelNode {
  public:
    enum WalkMode {
        INIT_GL,
        DRAW,
        DRAW_DEBUG,
        PICK,
        SELECT,
        DESELECT
    };

    static Matrix4x4 translateMatrix(const Vector4D& v);
    static Matrix4x4 scaleMatrix(const Vector4D& v);
    static Matrix4x4 rotateMatrix(char axis, double angle);

    explicit ModelNode(std::string desc = "");
    virtual ~ModelNode();

    ModelNode* getParent() const;
    ModelNode* getAncestor(std::string desc) const;
    void addChild(ModelNode *n);
    void walk(WalkMode mode);
    virtual void draw() const = 0;
    void transform(Matrix4x4 m);
    virtual void printTransform() const;

    virtual std::string print(bool root = true) const;

  protected:
    // Methods
    virtual void walkPush(WalkMode mode) = 0;
    virtual void walkBody(WalkMode mode);
    virtual void walkPop(WalkMode mode) = 0;
    void drawOrigin() const;

    // Members
    // Parent
    ModelNode* m_parent;
    // Children
    std::list<ModelNode*> m_childlist;
    // Current transform
    Matrix4x4 m_transform;
    // Readable description
    std::string m_desc;

  private:
    // Members
    // Debug output
    DebugStream& dout;
};

std::ostream& operator<<(std::ostream& os, const ModelNode& n);

#endif // MODELNODE_HPP
