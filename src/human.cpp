/*
 * human.cpp
 *
 * TODO
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#include "human.hpp"
#include "realnode.hpp"
#include "sphere.hpp"
#include "debugstream.hpp"
#include "leg.hpp"
#include "arm.hpp"
#include <cmath>
#include <iostream>

Human::Human(std::string desc) : VirtualNode(desc), dout(DebugManager::getInstance().newStream("Human")) {
    dout.enterScope("Human::Human");

    // Create the upper body real leaf relative to human's origin
    RealNode *upperBody = new RealNode(new Sphere(), "upperbody");
    upperBody->transform(ModelNode::translateMatrix(Vector4D(0, 2.5, 0, 0)));
    upperBody->transform(ModelNode::scaleMatrix(Vector4D(3.25, 3, 2.2, 0)));
    // Add as child to main body
    addChild(upperBody);

    // Create the lower body real leaf relative to human's origin
    RealNode *lowerBody = new RealNode(new Sphere(), "lowerbody");
    lowerBody->transform(ModelNode::translateMatrix(Vector4D(0, -1.5, 0, 0)));
    lowerBody->transform(ModelNode::scaleMatrix(Vector4D(3, 2.5, 2.2, 0)));
    // Add as child to the main body
    addChild(lowerBody);

    // Create the collar virtual child relative to human's origin
    m_collar = new VirtualNode("collar");
    m_collar->transform(ModelNode::translateMatrix(Vector4D(0, 5.5, 0, 0)));
    // Add as child to the main body
    addChild(m_collar);

    // Create the shoulders real leaf relative to collar's origin
    RealNode *shoulders = new RealNode(new Sphere(), "shoulders");
    shoulders->transform(ModelNode::scaleMatrix(Vector4D(4.5, 0.75, 2.2, 0)));
    // Add as child to the collar
    m_collar->addChild(shoulders);

    // Create the left arm virtual child relative to collar's origin
    m_leftArm = new Arm("LeftArm");
    m_leftArm->transform(ModelNode::translateMatrix(Vector4D(-4.5, 0, 0, 0)));
    // Add as child to the collar
    m_collar->addChild(m_leftArm);

    // Create the right arm virtual child relative to collar's origin
    m_rightArm = new Arm("RightArm");
    m_rightArm->transform(ModelNode::translateMatrix(Vector4D(4.5, 0, 0, 0)));
    // Add as child to the collar
    m_collar->addChild(m_rightArm);

    // Create the waist virtual child relative to human's origin
    m_waist = new VirtualNode("waist");
    m_waist->transform(ModelNode::translateMatrix(Vector4D(0, -4, 0, 0)));
    // Add as child to the main body
    addChild(m_waist);

    // Create the hips real leaf relative to waist's origin
    RealNode *hips = new RealNode(new Sphere(), "hips");
    hips->transform(ModelNode::scaleMatrix(Vector4D(3.75, 1.25, 2.2, 0)));
    // Add as child to the waist
    m_waist->addChild(hips);

    // Create the left leg virtual child relative to waist's origin
    m_leftLeg = new Leg("LeftLeg");
    m_leftLeg->transform(ModelNode::translateMatrix(Vector4D(-3, 0, 0, 0)));
    m_leftLeg->transform(ModelNode::rotateMatrix('y', M_PI / 2));
    // Add as child to the waist
    m_waist->addChild(m_leftLeg);

    // Create the right leg virtual child relative to waist's origin
    m_rightLeg = new Leg("RightLeg");
    m_rightLeg->transform(ModelNode::translateMatrix(Vector4D(3, 0, 0, 0)));
    m_rightLeg->transform(ModelNode::rotateMatrix('y', M_PI / 2));
    // Add as child to the waist
    m_waist->addChild(m_rightLeg);

    // Create the neck real leaf relative to collar's origin
    RealNode *neck = new RealNode(new Sphere(), "neck");
    neck->transform(ModelNode::translateMatrix(Vector4D(0, 0.5, 0, 0)));
    neck->transform(ModelNode::scaleMatrix(Vector4D(3, 0.85, 1, 0)));
    // Add as child to collar
    m_collar->addChild(neck);

    // Create the neck virtual child relative to collar's origin
    m_neck = new VirtualNode("neck");
    m_neck->transform(ModelNode::translateMatrix(Vector4D(0, 0.5, 0, 0)));
    // Add as child to collar
    m_collar->addChild(m_neck);

    // Create the head real leaf relative to neck's origin
    RealNode *head = new RealNode(new Sphere(), "head");
    head->transform(ModelNode::translateMatrix(Vector4D(0, 1.75, 0, 0)));
    head->transform(ModelNode::scaleMatrix(Vector4D(1.75, 1.75, 1.75, 0)));
    // Add as child to neck
    m_neck->addChild(head);

    // Initialize members
    m_shouldersTwistFactor = m_hipsTwistFactor = 0;

    dout.exitScope();
}

Human::~Human() {
    dout.enterScope("Human::~Human");
    dout.exitScope();
}

bool Human::flexNeck(double factor) {
    dout.enterScope("Human::flexNeck");

    if (m_neckFlexFactor + factor > M_PI / 4) {
        m_neck->transform(ModelNode::rotateMatrix('x', M_PI / 4 - m_neckFlexFactor));
        m_neckFlexFactor = M_PI / 4;
        dout.exitScope();
        return false;
    } else if (m_neckFlexFactor + factor < M_PI / -4) {
        m_neck->transform(ModelNode::rotateMatrix('x', M_PI / -4 - m_neckFlexFactor));
        m_neckFlexFactor = M_PI / -4;
        dout.exitScope();
        return false;
    } else {
        m_neck->transform(ModelNode::rotateMatrix('x', factor));
        m_neckFlexFactor += factor;
        dout.exitScope();
        return true;
    }
}

bool Human::twistNeck(double factor) {
    dout.enterScope("Human::twistNeck");

    if (m_neckTwistFactor + factor > M_PI / 2) {
        m_neck->transform(ModelNode::rotateMatrix('y', M_PI / 2 - m_neckTwistFactor));
        m_neckTwistFactor = M_PI / 2;
        dout.exitScope();
        return false;
    } else if (m_neckTwistFactor + factor < M_PI / -2) {
        m_neck->transform(ModelNode::rotateMatrix('y', M_PI / -2 - m_neckTwistFactor));
        m_neckTwistFactor = M_PI / -2;
        dout.exitScope();
        return false;
    } else {
        m_neck->transform(ModelNode::rotateMatrix('y', factor));
        m_neckTwistFactor += factor;
        dout.exitScope();
        return true;
    }
}

bool Human::twistShoulders(double factor) {
    dout.enterScope("Human::twistShoulders");

    if (m_shouldersTwistFactor + factor > M_PI / 6) {
        m_collar->transform(ModelNode::rotateMatrix('y', M_PI / 6 - m_shouldersTwistFactor));
        m_shouldersTwistFactor = M_PI / 6;
        dout.exitScope();
        return false;
    } else if (m_shouldersTwistFactor + factor < M_PI / -6) {
        m_collar->transform(ModelNode::rotateMatrix('y', M_PI / -6 - m_shouldersTwistFactor));
        m_shouldersTwistFactor = M_PI / -6;
        dout.exitScope();
        return false;
    } else {
        m_collar->transform(ModelNode::rotateMatrix('y', factor));
        m_shouldersTwistFactor += factor;
        dout.exitScope();
        return true;
    }
}

bool Human::flexLeftUpperarm(double factor) {
    dout.enterScope("Human::flexLeftUpperarm");
    dout.exitScope();
    return m_leftArm->flexUpperarm(factor);
}

bool Human::flexRightUpperarm(double factor) {
    dout.enterScope("Human::flexRightUpperarm");
    dout.exitScope();
    return m_rightArm->flexUpperarm(factor);
}

bool Human::flexLeftForearm(double factor) {
    dout.enterScope("Human::flexLeftForearm");
    dout.exitScope();
    return m_leftArm->flexForearm(factor);
}

bool Human::flexRightForearm(double factor) {
    dout.enterScope("Human::flexRightForearm");
    dout.exitScope();
    return m_rightArm->flexForearm(factor);
}

bool Human::flexLeftHand(double factor) {
    dout.enterScope("Human::flexLeftHand");
    dout.exitScope();
    return m_leftArm->flexHand(factor);
}

bool Human::flexRightHand(double factor) {
    dout.enterScope("Human::flexRightHand");
    dout.exitScope();
    return m_rightArm->flexHand(factor);
}

bool Human::twistHips(double factor) {
    dout.enterScope("Human::twistHips");

    if (m_hipsTwistFactor + factor > M_PI / 6) {
        m_waist->transform(ModelNode::rotateMatrix('y', M_PI / 6 - m_hipsTwistFactor));
        m_hipsTwistFactor = M_PI / 6;
        dout.exitScope();
        return false;
    } else if (m_hipsTwistFactor + factor < M_PI / -6) {
        m_waist->transform(ModelNode::rotateMatrix('y', M_PI / -6 - m_hipsTwistFactor));
        m_hipsTwistFactor = M_PI / -6;
        dout.exitScope();
        return false;
    } else {
        m_waist->transform(ModelNode::rotateMatrix('y', factor));
        m_hipsTwistFactor += factor;
        dout.exitScope();
        return true;
    }
}

bool Human::flexLeftThigh(double factor) {
    dout.enterScope("Human::flexLeftThigh");
    dout.exitScope();
    return m_leftLeg->flexThigh(factor);
}

bool Human::flexRightThigh(double factor) {
    dout.enterScope("Human::flexRightThigh");
    dout.exitScope();
    return m_rightLeg->flexThigh(factor);
}

bool Human::flexLeftCalf(double factor) {
    dout.enterScope("Human::flexLeftCalf");
    dout.exitScope();
    return m_leftLeg->flexCalf(factor);
}

bool Human::flexRightCalf(double factor) {
    dout.enterScope("Human::flexRightCalf");
    dout.exitScope();
    return m_rightLeg->flexCalf(factor);
}

bool Human::flexLeftFoot(double factor) {
    dout.enterScope("Human::flexLeftFoot");
    dout.exitScope();
    return m_leftLeg->flexFoot(factor);
}

bool Human::flexRightFoot(double factor) {
    dout.enterScope("Human::flexRightFoot");
    dout.exitScope();
    return m_rightLeg->flexFoot(factor);
}

void Human::printTransform() const {
    m_leftLeg->printTransform();
}
