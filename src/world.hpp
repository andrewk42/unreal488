/*
 * world.hpp
 *
 * The common root-level model object
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#ifndef WORLD_HPP
#define WORLD_HPP

#include "virtualnode.hpp"
#include <set>

class Player;

class World : public VirtualNode {
  public:
    World();
    virtual ~World();

    void addPlayer(Player* p);

  private:
    // Members
    // Shadow Player list for convenience
    std::set<Player*> m_playerList;
    // Debug output
    DebugStream& dout;
};

#endif // WORLD_HPP
