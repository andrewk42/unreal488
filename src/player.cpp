/*
 * player.cpp
 *
 * TODO
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#include "player.hpp"
#include "realnode.hpp"
#include "sphere.hpp"
#include "debugstream.hpp"
#include "human.hpp"

Player::Player() : dout(DebugManager::getInstance().newStream("Player")) {
    dout.enterScope("Player::Player");

    dout.exitScope();
}

Player::~Player() {
    dout.enterScope("Player::~Player");
    dout.exitScope();
}
