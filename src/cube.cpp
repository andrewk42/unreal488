/*
 * cube.cpp
 *
 * TODO
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#include "cube.hpp"
#include "debugstream.hpp"
#include <cmath>

unsigned Cube::m_displayListIndex = 0;

Cube::Cube(unsigned length) : dout(DebugManager::getInstance().newStream("Cube")) {
    dout.enterScope("Cube::Cube");

    m_length = length;

    dout.exitScope();
}

Cube::~Cube() {
    dout.enterScope("Cube::~Cube");
    dout.exitScope();
}

void Cube::initGL() {
    dout.enterScope("Cube::initDisplayList");

    if (m_displayListIndex != 0) {
        return;
    }

    // Get a new display list index from OpenGL
    m_displayListIndex = glGenLists(1);

    /* 
     * The following draws a 3D cube using GL_QUADS
     * 
     * I use QL_QUADS here because of specifying normals. Although
     * QL_QUAD_STRIPS or FANs are faster, I want normals to be well
     * defined at each vertex, so all 24 are specified.
     */

    double radius = (double)m_length / 2;

    glNewList(m_displayListIndex, GL_COMPILE);
        glBegin(GL_QUADS);
            // Draw front face
            glVertex3d(-radius, -radius, radius);
            glNormal3d(0, 0, 1);
            glVertex3d(radius, -radius, radius);
            glNormal3d(0, 0, 1);
            glVertex3d(radius, radius, radius);
            glNormal3d(0, 0, 1);
            glVertex3d(-radius, radius, radius);
            glNormal3d(0, 0, 1);

            // Draw top face
            glVertex3d(-radius, radius, radius);
            glNormal3d(0, 1, 0);
            glVertex3d(radius, radius, radius);
            glNormal3d(0, 1, 0);
            glVertex3d(radius, radius, -radius);
            glNormal3d(0, 1, 0);
            glVertex3d(-radius, radius, -radius);
            glNormal3d(0, 1, 0);

            // Draw bottom face
            glVertex3d(-radius, -radius, radius);
            glNormal3d(0, -1, 0);
            glVertex3d(-radius, -radius, -radius);
            glNormal3d(0, -1, 0);
            glVertex3d(radius, -radius, -radius);
            glNormal3d(0, -1, 0);
            glVertex3d(radius, -radius, radius);
            glNormal3d(0, -1, 0);

            // Draw left face
            glVertex3d(-radius, -radius, radius);
            glNormal3d(-1, 0, 0);
            glVertex3d(-radius, radius, radius);
            glNormal3d(-1, 0, 0);
            glVertex3d(-radius, radius, -radius);
            glNormal3d(-1, 0, 0);
            glVertex3d(-radius, -radius, -radius);
            glNormal3d(-1, 0, 0);

            // Draw right face
            glVertex3d(radius, -radius, radius);
            glNormal3d(1, 0, 0);
            glVertex3d(radius, -radius, -radius);
            glNormal3d(1, 0, 0);
            glVertex3d(radius, radius, -radius);
            glNormal3d(1, 0, 0);
            glVertex3d(radius, radius, radius);
            glNormal3d(1, 0, 0);

            // Draw back face
            glVertex3d(-radius, -radius, -radius);
            glNormal3d(0, 0, 1);
            glVertex3d(-radius, radius, -radius);
            glNormal3d(0, 0, 1);
            glVertex3d(radius, radius, -radius);
            glNormal3d(0, 0, 1);
            glVertex3d(radius, -radius, -radius);
            glNormal3d(0, 0, 1);
        glEnd();
    glEndList();

    dout.exitScope();
}

void Cube::draw() const {
    dout.enterScope("Cube::draw");

    if (m_displayListIndex != 0) {
        glCallList(m_displayListIndex);
    } else {
        dout << "Error: Cube has no display list" << std::endl;
    }

    dout.exitScope();
}

std::string Cube::print() const {
    return "[Cube]";
}

std::ostream& operator<<(std::ostream& os, const Cube& s) {
    return os << s.print();
}
