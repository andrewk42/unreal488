/*
 * primitive.hpp
 *
 * An object that represents a "simple" graphical "shape". RealNodes
 * will be composed of these primitives, which may be altered to suit
 * the specific needs of the model.
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#ifndef PRIMITIVE_HPP
#define PRIMITIVE_HPP

#include <ostream>

class DebugStream;

class Primitive {
  public:
    Primitive();
    virtual ~Primitive();

    virtual void initGL() = 0;
    virtual void draw() const = 0;
    virtual std::string print() const = 0;

  private:
    // Members
    // Debug output
    DebugStream& dout;
};

std::ostream& operator<<(std::ostream& os, const Primitive& p);

#endif // PRIMITIVE_HPP
