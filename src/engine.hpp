/*
 * engine.hpp
 *
 * This object manages gamestate and the model tree.
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#ifndef ENGINE_HPP
#define ENGINE_HPP

#include "viewport.hpp"
#include <map>

class ModelNode;
class RealNode;
class VirtualNode;
class Trackball;
class DebugStream;
class Player;

class EngineException : public std::runtime_error {
  public:
    EngineException(std::string msg = "EngineException");
};

class Engine {
  public:
    enum GameType {
        NONE,
        DESIGN,
        PLAY,
    };

    enum TransformMode {
        TRANSLATE,
        SCALE,
        ROTATE,
    };

    struct GameInput {
        // Constructor
        GameInput();

        // Mouse stuff
        int newX, oldX, newY, oldY;
        std::map<std::string, bool> mouseButtons;

        // Keyboard keys
        std::map<std::string, bool> keys;
    };

    Engine(Viewport& v, Trackball& t);
    virtual ~Engine();
    ModelNode* init(Engine::GameType t);
    Engine::GameType getCurrentType() const;
    void input(GameInput i);

    // Slots
    void onPicked(Viewport::PickResponse response);

  private:
    // Methods
    // Designer stuff
    void setupDesigner();
    void inputDesigner(GameInput i);
    void translateWorldXY(int deltaX, int deltaY);
    void translateWorldZ(int deltaY);
    void rotateWorld(int newX, int newY, int oldX, int oldY);
    void transformModelX(TransformMode m, int deltaX);
    void transformModelY(TransformMode m, int deltaY);
    void transformModelZ(TransformMode m, int deltaY);
    void pickDesigner(Viewport::PickResponse response);
    void animateModel(bool reverse);

    // Game stuff

    // Members
    // Game type
    Engine::GameType m_gameType;
    // Model root
    ModelNode* m_modelRoot;
    // Viewport reference
    Viewport& m_viewport;
    // Trackball reference
    Trackball& m_trackball;
    // Currently picked model
    RealNode *m_currentPick;
    VirtualNode *m_complexPick;
    // Debug output
    DebugStream& dout;
};

#endif // ENGINE_HPP
