/*
 * debugstream.cpp
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#include "debugstream.hpp"
#include "gamesettings.hpp"
#include <ctime>
#include <sstream>

/*
 * DebugException definitions
 */

DebugException::DebugException(std::string msg) : std::runtime_error(msg) {

}


/*
 * DebugStream definitions
 */

DebugStream::DebugStream(std::string className) : std::ostream(&m_buffer), m_buffer(className) {

}

void DebugStream::enterScope(std::string scopeName) {
    std::stringbuf b("Entering " + scopeName + "...\n");

    operator<<(&b);
    m_buffer.sync();
    m_buffer.m_scope.push(scopeName);
}

void DebugStream::exitScope() {
    std::stringbuf b("Exiting " + m_buffer.m_scope.top() + "...\n");

    m_buffer.m_scope.pop();
    operator<<(&b);
    m_buffer.sync();
}

DebugStream::DebugBuf::DebugBuf(std::string fileName) {
    // Get logfile path details from GameSettings
    GameSettings &s = GameSettings::getInstance();
    std::string name = s[GameSettings::LOGFILE_PATH_PREFIX] + fileName + s[GameSettings::LOGFILE_PATH_SUFFIX];

    // Get timestamp
    time_t t = time(0);
    tm *now = localtime(&t);
    std::stringstream timestamp;
    timestamp << (now->tm_year + 1900) << '-' << (now->tm_mon + 1) << '-' << now->tm_mday
                << " @ " << now->tm_hour << ":" << now->tm_min << ":" << now->tm_sec;

    // Begin writing to file, noting the last call to open()
    m_outFile.open(name.c_str(), std::fstream::app);
    if (!m_outFile.fail()) {
        m_outFile << "========== File opened on " << timestamp.str() << " ==========\n";
        m_outFile.flush();
    } else {
        throw DebugException("Couldn't write to \"" + name + "\", does it exist?");
    }
}

DebugStream::DebugBuf::~DebugBuf() {
    m_outFile.close();
}

int DebugStream::DebugBuf::sync() {
    // Add space to account for scope depth
    for (unsigned i = 0; i < m_scope.size(); i++) {
        m_outFile << "  ";
    }

    // Prepend with timestamp
    m_outFile << "[" << DebugManager::getInstance().nextSequence() << "] ";
    m_outFile << str();
    str("");
    m_outFile.flush();
    return 0;
}


/*
 * DebugManager definitions
 */

DebugManager& DebugManager::getInstance() {
    static DebugManager instance;

    return instance;
}

DebugManager::~DebugManager() {
    for (std::map<std::string, DebugStream*>::const_iterator it = m_streams.begin(); it != m_streams.end(); it++) {
        delete it->second;
    }
}

DebugStream& DebugManager::newStream(std::string name) {
    // Reuse streams
    if (m_streams.count(name) > 0) {
        return *m_streams[name];
    }

    DebugStream *s = new DebugStream(name);
    m_streams.insert(std::pair<std::string, DebugStream*>(name, s));

    return *s;
}

unsigned DebugManager::nextSequence() {
    return m_sequence++;
}

DebugManager::DebugManager() {
    m_sequence = 0;
}
