/*
 * colour.hpp
 *
 * 
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#ifndef COLOUR_HPP
#define COLOUR_HPP

#include <ostream>

class DebugStream;

class Colour {
  public:
    enum Preset {
        BLACK, // 0 0 0
        RED, // 1 0 0
        GREEN, // 0 1 0
        BLUE, // 0 0 1
        YELLOW, // 1 1 0
        MAGENTA, // 1 0 1
        CYAN, // 0 1 1
        WHITE // 1 1 1
    };

    explicit Colour(Colour::Preset p);
    Colour(double r, double g, double b);
    Colour(const Colour& other);
    Colour& operator=(const Colour& other);

    double R() const;
    double G() const;
    double B() const;

    Colour operator*(double s) const;

  private:
    // Members
    // Data
    double m_red, m_green, m_blue;
    // Debug output
    DebugStream& dout;
};

Colour operator*(double s, const Colour& c);
Colour operator*(const Colour& c1, const Colour& c2);
Colour operator+(const Colour& c1, const Colour& c2);
bool operator==(const Colour& c1, const Colour& c2);
std::ostream& operator<<(std::ostream& os, const Colour& c);

#endif // COLOUR_HPP
