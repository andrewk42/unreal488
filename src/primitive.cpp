/*
 * primitive.cpp
 *
 * TODO
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#include "primitive.hpp"
#include "debugstream.hpp"

Primitive::Primitive() : dout(DebugManager::getInstance().newStream("Primitive")) {
    dout.enterScope("Primitive::Primitive");
    dout.exitScope();
}

Primitive::~Primitive() {
    dout.enterScope("Primitive::~Primitive");
    dout.exitScope();
}

std::ostream& operator<<(std::ostream& os, const Primitive& p) {
    return os << p.print();
}
