/*
 * cube.hpp
 *
 * The Cube primitive.
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#ifndef CUBE_HPP
#define CUBE_HPP

#include "primitive.hpp"
#include <GL/gl.h>

class Cube : public Primitive {
  public:
    Cube(unsigned length = 1);
    virtual ~Cube();

    virtual void initGL();
    virtual void draw() const;
    virtual std::string print() const;

  private:
    // Members
    // OpenGL Display list index
    static GLuint m_displayListIndex;
    // Drawing parameters
    unsigned m_length;
    // Debug output
    DebugStream& dout;
};

std::ostream& operator<<(std::ostream& os, const Cube& s);

#endif // CUBE_HPP
