/*
 * trackball.hpp
 *
 * Used to "translate" a user's "mouselook" into a rotation matrix
 * to be applied to the model.
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#ifndef TRACKBALL_H
#define TRACKBALL_H

#include "algebra.hpp"
#include <gtkmm.h>

class Trackball {
  public:
    Trackball();
    virtual ~Trackball();

    Matrix4x4 getTransform(unsigned newX, unsigned newY, unsigned oldX, unsigned oldY) const;

    // Slots
    // Must be called when the viewport is resized
    virtual bool viewportResized(GdkEventConfigure* event);

  private:
    // Methods
    Vector4D getVector(double x, double y) const;

    // Members
    // Trackball radius
    unsigned m_radius, m_viewportWidth, m_viewportHeight;
    // Debug output
    DebugStream& dout;
};

#endif // TRACKBALL_H
