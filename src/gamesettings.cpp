/*
 * gamesettings.cpp
 *
 * The values are split up into different map members by type.
 * the operator[] will pick the map based on the enum's group
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#include "gamesettings.hpp"

GameSettings& GameSettings::getInstance() {
    static GameSettings instance;

    return instance;
}

unsigned& GameSettings::operator[](const GameSettings::UnsignedSetting& key) {
    return m_unsigned[key];
}

std::string& GameSettings::operator[](const GameSettings::StringSetting& key) {
    return m_string[key];
}

GameSettings::GameSettings() {
    // Manually insert default values
    // Unsigneds
    m_unsigned.insert(std::pair<GameSettings::UnsignedSetting, unsigned>(
        GameSettings::VIEWPORT_WIDTH, GameDefaults::VIEWPORT_WIDTH));
    m_unsigned.insert(std::pair<GameSettings::UnsignedSetting, unsigned>(
        GameSettings::VIEWPORT_HEIGHT, GameDefaults::VIEWPORT_HEIGHT));
    m_unsigned.insert(std::pair<GameSettings::UnsignedSetting, unsigned>(
        GameSettings::INVALIDATE_TIMEOUT_MS, GameDefaults::INVALIDATE_TIMEOUT_MS));
    m_unsigned.insert(std::pair<GameSettings::UnsignedSetting, unsigned>(
        GameSettings::PICK_BUFFER_SIZE, GameDefaults::PICK_BUFFER_SIZE));

    // Strings
    m_string.insert(std::pair<GameSettings::StringSetting, std::string>(
        GameSettings::LOGFILE_PATH_PREFIX, GameDefaults::LOGFILE_PATH_PREFIX));
    m_string.insert(std::pair<GameSettings::StringSetting, std::string>(
        GameSettings::LOGFILE_PATH_SUFFIX, GameDefaults::LOGFILE_PATH_SUFFIX));
}

GameSettings::~GameSettings() {

}
