/*
 * human.hpp
 *
 * 
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#ifndef HUMAN_HPP
#define HUMAN_HPP

#include "virtualnode.hpp"

class Leg;
class Arm;

class Human : public VirtualNode {
  public:
    explicit Human(std::string desc = "");
    virtual ~Human();

    // Movement
    // Neck
    bool flexNeck(double factor);
    bool twistNeck(double factor);
    // Shoulders
    bool twistShoulders(double factor);
    // Arms
    bool flexLeftUpperarm(double factor);
    bool flexRightUpperarm(double factor);
    bool flexLeftForearm(double factor);
    bool flexRightForearm(double factor);
    bool flexLeftHand(double factor);
    bool flexRightHand(double factor);
    // Hips
    bool twistHips(double factor);
    // Legs
    bool flexLeftThigh(double factor);
    bool flexRightThigh(double factor);
    bool flexLeftCalf(double factor);
    bool flexRightCalf(double factor);
    bool flexLeftFoot(double factor);
    bool flexRightFoot(double factor);

    virtual void printTransform() const;

  protected:
    // Members
    // Body parts
    VirtualNode *m_collar, *m_waist, *m_neck;
    Arm *m_leftArm, *m_rightArm;
    Leg *m_leftLeg, *m_rightLeg;
    double m_neckTwistFactor, m_neckFlexFactor, m_shouldersTwistFactor, m_hipsTwistFactor;

  private:
    // Members
    // Debug output
    DebugStream& dout;
};

#endif // HUMAN_HPP
