/*
 * realnode.hpp
 *
 * A type of ModelNode that represents a physical, tangible
 * object. Its key property is having geometric primitives
 * i.e. a graphical representation that the user will see
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#ifndef REALNODE_HPP
#define REALNODE_HPP

#include "modelnode.hpp"
#include "colour.hpp"
#include <map>

class Primitive;

class RealNode : public ModelNode {
  public:
    static void resetPickingNames();
    static RealNode* getByPickingName(unsigned name);

    explicit RealNode(Primitive *p, std::string desc = "");
    virtual ~RealNode();

    virtual void draw() const;
    virtual void select(bool selected = true);

    virtual std::string print(bool root = true) const;

  protected:
    // Methods
    virtual void walkPush(WalkMode mode);
    virtual void walkBody(WalkMode mode);
    virtual void walkPop(WalkMode mode);

    // Members
    // Picking names
    static std::map<unsigned, RealNode*> m_pickingNames;
    // Primitive reference
    Primitive* m_primitive;
    // Base colour
    Colour m_baseColour;
    // Picking name
    unsigned m_pickName;

  private:
    // Members
    // Debug output
    DebugStream& dout;
};

std::ostream& operator<<(std::ostream& os, const RealNode& n);

#endif // REALNODE_HPP
