/*
 * viewport.hpp
 *
 * This is the game viewport. It uses OpenGL to repeatedly draw the
 * Model tree. It is also what listens for user input from the mouse
 * and keyboard, which are forwarded to the GameWindow via gtk signals
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#ifndef VIEWPORT_HPP
#define VIEWPORT_HPP

#include "algebra.hpp"
#include <gtkmm.h>
#include <gtkglmm.h>
#include <stdexcept>
#include <list>

class ModelNode;
class RealNode;
class DebugStream;

class ViewportException : public std::runtime_error {
  public:
    ViewportException(std::string msg = "ViewportException");
};

class Viewport : public Gtk::GL::DrawingArea {
  public:
    struct PickRequest {
        int x, y;
        unsigned w, h;
    };

    struct PickResponse {
        std::list<RealNode*> models;
    };

    Viewport();
    virtual ~Viewport();

    // Methods
    // Access picked signal
    sigc::signal<void, PickResponse> pickedSignal() const;
    // A useful function that forces this widget to rerender. This should
    // be called instead of on_expose_event directly
    bool invalidate();
    // The root model node that the Viewer will traverse
    void attachModel(ModelNode *root);
    // Turn wireframe mode on/off
    void toggleWireFrame();
    // Turn backface culling on/off
    void toggleBackCull();
    // Turn frontface culling on/off
    void toggleFrontCull();
    // Picking
    void pick(int x, int y, unsigned w, unsigned h);

  protected:
    // Members
    // Signals
    sigc::signal<void, PickResponse> sig_modelPicked;

    // Event slots
    // Called when GL is first initialized
    virtual void on_realize();
    // Called when the viewport's contents need to be redrawn
    virtual bool on_expose_event(GdkEventExpose* event);
    // Called when the window is resized
    virtual bool on_configure_event(GdkEventConfigure* event);

  private:
    // Methods
    void processHits(GLuint hits);

    // Members
    // Aspect ratio
    GLfloat m_aspect;
    // Model tree
    ModelNode* m_modelRoot;
    // Picking buffer
    GLuint *m_pickBuffer;
    // Picking trigger
    PickRequest *m_pickRequest;
    // Option states
    bool m_wireframe, m_backcull, m_frontcull;
    // Debug output
    DebugStream& dout;
};

#endif // VIEWPORT_HPP
