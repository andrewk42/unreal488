/*
 * gamesettings.hpp
 *
 * These are predefined constants used in various parts of
 * the program. Each constant will have a default defined in
 * the GameDefaults namespace, and each constant can be
 * overwritten with a custom value via the operator[].
 *
 * Note: Must be initialized in main() before anything
 * else, because some object constructors will reference these.
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#ifndef GAMESETTINGS_HPP
#define GAMESETTINGS_HPP

#include <string>
#include <map>

namespace GameDefaults {
    const std::string LOGFILE_PATH_PREFIX = "../log/"; // DebugStream
    const std::string LOGFILE_PATH_SUFFIX = ".log"; // DebugStream
    const unsigned VIEWPORT_WIDTH = 800; // Viewport
    const unsigned VIEWPORT_HEIGHT = 600; // Viewport
    const unsigned INVALIDATE_TIMEOUT_MS = 50; // Viewport
    const unsigned PICK_BUFFER_SIZE = 50;
}

class GameSettings {
  public:
    enum UnsignedSetting {
        VIEWPORT_WIDTH, // Viewport
        VIEWPORT_HEIGHT, // Viewport
        INVALIDATE_TIMEOUT_MS, // Viewport
        PICK_BUFFER_SIZE,
    };

    enum StringSetting {
        LOGFILE_PATH_PREFIX, // DebugStream
        LOGFILE_PATH_SUFFIX, // DebugStream
    };

    static GameSettings& getInstance();
    virtual ~GameSettings();

    unsigned& operator[](const UnsignedSetting& key);
    std::string& operator[](const StringSetting& key);

  private:
    typedef std::map<UnsignedSetting, unsigned> UnsignedMap;
    typedef std::map<StringSetting, std::string> StringMap;

    // Methods
    // Singleton stuff
    GameSettings();
    GameSettings(const GameSettings& other);
    void operator=(const GameSettings& other);

    // Members
    UnsignedMap m_unsigned;
    StringMap m_string;
};

#endif // GAMESETTINGS_HPP
