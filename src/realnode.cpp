/*
 * realnode.cpp
 *
 * TODO
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#include "realnode.hpp"
#include "primitive.hpp"
#include "debugstream.hpp"
#include <GL/gl.h>
#include <sstream>
#include <iostream>

void RealNode::resetPickingNames() {
    DebugStream &dout = DebugManager::getInstance().newStream("RealNode");
    dout.enterScope("RealNode::resetPickingNames");

    m_pickingNames.clear();

    dout.exitScope();
}

RealNode* RealNode::getByPickingName(unsigned name) {
    DebugStream &dout = DebugManager::getInstance().newStream("RealNode");
    dout.enterScope("RealNode::getByPickingName");

    dout.exitScope();
    return m_pickingNames[name];
}

std::map<unsigned, RealNode*> RealNode::m_pickingNames;

RealNode::RealNode(Primitive *p, std::string desc) : ModelNode(desc), m_primitive(p), m_baseColour(Colour::WHITE), dout(DebugManager::getInstance().newStream("RealNode")) {
    dout.enterScope("RealNode::RealNode");

    dout << "Creating RealNode with primitive " << *p << std::endl;

    dout.exitScope();
}

RealNode::~RealNode() {
    dout.enterScope("RealNode::~RealNode");

    delete m_primitive;

    dout.exitScope();
}

void RealNode::draw() const {
    dout.enterScope("RealNode::draw");

    // Set colour
    glColor3d(m_baseColour.R(), m_baseColour.G(), m_baseColour.B());

    // Call primitive draw code
    m_primitive->draw();

    dout.exitScope();
}

void RealNode::select(bool selected) {
    dout.enterScope("RealNode::select");

    if (selected) {
        m_baseColour = Colour(Colour::CYAN);
    } else {
        m_baseColour = Colour(Colour::WHITE);
    }

    dout.exitScope();
}

std::string RealNode::print(bool root) const {
    std::stringstream s;

    if (root) {
        s << "[RealNode|";
    }

    if (m_desc == "") {
        s << "primitive=" << *m_primitive << "|" << ModelNode::print(false);
    } else {
        s << ModelNode::print(false);
    }

    return s.str();
}

void RealNode::walkPush(WalkMode mode) {
    dout.enterScope("RealNode::walkPush");

    if (mode == DRAW_DEBUG) {
        Colour red = Colour(Colour::RED);

        glColor3d(red.R(), red.G(), red.B());
        ModelNode::drawOrigin();
        glColor3d(m_baseColour.R(), m_baseColour.G(), m_baseColour.B());
    }

    // Apply this node's transforms before drawing primitive
    glPushMatrix();
    glMultMatrixd(m_transform.transpose().begin());

    // Picking code
    if (mode == ModelNode::PICK) {
        m_pickName = m_pickingNames.size() + 1;
        m_pickingNames.insert(std::pair<unsigned, RealNode*>(m_pickName, this));
        dout << "Pushing name " << m_pickName << std::endl;
        glPushName(m_pickName);
    } else {
        m_pickName = 0;
    }

    dout.exitScope();
}

void RealNode::walkBody(WalkMode mode) {
    dout.enterScope("RealNode::walkBody");

    switch (mode) {
      case ModelNode::INIT_GL:
        dout << "Got INIT_GL operation" << std::endl;
        m_primitive->initGL();
        break;
      case ModelNode::DRAW_DEBUG:
      case ModelNode::PICK:
      case ModelNode::DRAW:
        draw();
        break;
      case ModelNode::SELECT:
        select(true);
        break;
      case ModelNode::DESELECT:
        select(false);
        break;
    }

    dout.exitScope();
}

void RealNode::walkPop(WalkMode mode) {
    dout.enterScope("RealNode::walkPop");

    // Picking code
    if (mode == ModelNode::PICK) {
        glPopName();
        dout << "Popping name " << std::endl;
    }

    glPopMatrix();

    dout.exitScope();
}

std::ostream& operator<<(std::ostream& os, const RealNode& n) {
    return os << n.print();
}
