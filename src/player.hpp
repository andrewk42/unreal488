/*
 * player.hpp
 *
 * The player's character model.
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "virtualnode.hpp"

class Player : public VirtualNode {
  public:
    Player();
    virtual ~Player();

  private:
    DebugStream& dout;
};

#endif // PLAYER_HPP
