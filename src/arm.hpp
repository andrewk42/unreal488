/*
 * arm.hpp
 *
 * 
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#ifndef ARM_HPP
#define ARM_HPP

#include "virtualnode.hpp"

class Arm : public VirtualNode {
  public:
    explicit Arm(std::string desc = "");
    virtual ~Arm();

    bool flexUpperarm(double factor);
    bool flexForearm(double factor);
    bool flexHand(double factor);

  protected:
    VirtualNode *m_wrist, *m_elbow;
    double m_upperarmFlexFactor, m_forearmFlexFactor, m_handFlexFactor;

  private:
    DebugStream& dout;
};

#endif // ARM_HPP
