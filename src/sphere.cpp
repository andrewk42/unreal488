/*
 * sphere.cpp
 *
 * TODO
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#include "sphere.hpp"
#include "debugstream.hpp"
#include <cmath>

unsigned Sphere::m_displayListIndex = 0;

Sphere::Sphere(unsigned radius, unsigned slices, unsigned stacks) : dout(DebugManager::getInstance().newStream("Sphere")) {
    dout.enterScope("Sphere::Sphere");

    m_radius = radius;
    m_slices = slices;
    m_stacks = stacks;

    dout.exitScope();
}

Sphere::~Sphere() {
    dout.enterScope("Sphere::~Sphere");
    dout.exitScope();
}

void Sphere::initGL() {
    dout.enterScope("Sphere::initDisplayList");

    if (m_displayListIndex != 0) {
        return;
    }

    // Get a new display list index from OpenGL
    m_displayListIndex = glGenLists(1);

    /* The following draws a 3D sphere using GL_QUAD_STRIP
     * 
     * When using GL_QUAD_STRIP, GL will draw a GL_QUAD using every
     * adjacent pair of points (each pair participates in 2 QUADs)
     *
     * If the vertices are listed as n = 1, n = 2, ..., n = N/2 - 1
     * then quadrilateral n will have the vertex order 2n-1, 2n, 2n+2,
     * 2n+1. e.g. the first GL_QUAD will be drawn using vertices 1, 2,
     * 4, then 3 in counter-clockwise order. The second GL_QUAD will be
     * drawn using vertices 3, 4, 6, then 5.
     *
     * To draw a sphere, start with equation x^2 + y^2 + z^2 = r^2
     * Rewrite this in parametric form using
     * x = r * sin(theta) * cos(phi)
     * y = r * sin(phi)
     * z = r * cos(theta) * cos(phi)
     *
     * where -PI/2 <= phi <= PI/2
     * and -PI <= theta <= PI
     *
     * Idea is to loop through theta and phi, stepping more often
     * within each angle as stacks and slices increase respectively.
     *
     * Since each drawn point is relative to the origin and we are drawing
     * in a spherical fasion around it, each normal is equal to its
     * respective 3D point
     */

    double x, y, z;

    glNewList(m_displayListIndex, GL_COMPILE);
        for (double theta = -M_PI; theta <= M_PI; theta += M_PI/m_slices) {
            glBegin(GL_QUAD_STRIP);
            for (double phi = -M_PI/2; phi <= M_PI/2; phi += M_PI/m_stacks) {
                x = m_radius * sin(theta) * cos(phi);
                y = m_radius * sin(phi);
                z = m_radius * cos(theta) * cos(phi);
                glVertex3d(x, y, z);
                glNormal3d(x, y, z);

                x = m_radius * sin(theta + M_PI/m_slices) * cos(phi);
                y = m_radius * sin(phi);
                z = m_radius * cos(theta + M_PI/m_slices) * cos(phi);
                glVertex3d(x, y, z);
                glNormal3d(x, y, z);
            }
            glEnd();
        }
    glEndList();

    dout.exitScope();
}

void Sphere::draw() const {
    dout.enterScope("Sphere::draw");

    if (m_displayListIndex != 0) {
        glCallList(m_displayListIndex);
    } else {
        dout << "Error: Sphere has no display list" << std::endl;
    }

    dout.exitScope();
}

std::string Sphere::print() const {
    return "[Sphere]";
}

std::ostream& operator<<(std::ostream& os, const Sphere& s) {
    return os << s.print();
}
