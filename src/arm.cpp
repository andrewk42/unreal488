/*
 * arm.cpp
 *
 * TODO
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#include "arm.hpp"
#include "debugstream.hpp"
#include "realnode.hpp"
#include "sphere.hpp"
#include <cmath>
#include <iostream>

Arm::Arm(std::string desc) : VirtualNode(desc), dout(DebugManager::getInstance().newStream("Arm")) {
    dout.enterScope("Arm::Arm");

    std::string namePrefix = "";
    if (m_desc != "") {
        namePrefix = m_desc + "|";
    }

    // Create the upperarm real leaf relative arm's origin
    RealNode *upperarm = new RealNode(new Sphere(), namePrefix + "upperarm");
    upperarm->transform(ModelNode::translateMatrix(Vector4D(0, -2.5, 0, 0)));
    upperarm->transform(ModelNode::scaleMatrix(Vector4D(1.05, 2.5, 0.8, 0)));
    // Add as child to the main arm
    addChild(upperarm);

    // Create the elbow virtual child relative arm's origin
    m_elbow = new VirtualNode(namePrefix + "elbow");
    m_elbow->transform(ModelNode::translateMatrix(Vector4D(0, -4.5, 0, 0)));
    // Add as child to the main arm
    addChild(m_elbow);

    // Create the forearm real leaf relative elbow's origin
    RealNode *forearm = new RealNode(new Sphere(), namePrefix + "forearm");
    forearm->transform(ModelNode::translateMatrix(Vector4D(0, -2, 0, 0)));
    forearm->transform(ModelNode::scaleMatrix(Vector4D(0.75, 2, 0.8, 0)));
    // Add as child to the elbow
    m_elbow->addChild(forearm);

    // Create the wrist virtual child relative elbow's origin
    m_wrist = new VirtualNode(namePrefix + "wrist");
    m_wrist->transform(ModelNode::translateMatrix(Vector4D(0, -4, 0, 0)));
    // Add as child to the elbow
    m_elbow->addChild(m_wrist);

    // Create the hand real leaf relative wrist's origin
    RealNode *hand = new RealNode(new Sphere(), namePrefix + "hand");
    hand->transform(ModelNode::translateMatrix(Vector4D(0, -0.5, 0, 0)));
    hand->transform(ModelNode::scaleMatrix(Vector4D(0.5, 0.5, 0.5, 0)));
    // Add as child to the wrist
    m_wrist->addChild(hand);

    // Initialize members
    m_upperarmFlexFactor = m_forearmFlexFactor = m_handFlexFactor = 0;

    dout.exitScope();
}

Arm::~Arm() {
    dout.enterScope("Arm::~Arm");

    dout.exitScope();
}

bool Arm::flexUpperarm(double factor) {
    dout.enterScope("Arm::flexUpperarm");

    if (m_upperarmFlexFactor + factor > M_PI / 2) {
        transform(ModelNode::rotateMatrix('x', M_PI / 2 - m_upperarmFlexFactor));
        m_upperarmFlexFactor = M_PI / 2;
        dout.exitScope();
        return false;
    } else if (m_upperarmFlexFactor + factor < -M_PI) {
        transform(ModelNode::rotateMatrix('x', -M_PI - m_upperarmFlexFactor));
        m_upperarmFlexFactor = -M_PI;
        dout.exitScope();
        return false;
    } else {
        transform(ModelNode::rotateMatrix('x', factor));
        m_upperarmFlexFactor += factor;
        dout.exitScope();
        return true;
    }
}

bool Arm::flexForearm(double factor) {
    dout.enterScope("Arm::flexForearm");

    if (m_forearmFlexFactor + factor > 0) {
        m_elbow->transform(ModelNode::rotateMatrix('x', 0 - m_forearmFlexFactor));
        m_forearmFlexFactor = 0;
        dout.exitScope();
        return false;
    } else if (m_forearmFlexFactor + factor < 7 * M_PI / -8) {
        m_elbow->transform(ModelNode::rotateMatrix('x', (7 * M_PI / -8) - m_forearmFlexFactor));
        m_forearmFlexFactor = 7 * M_PI / -8;
        dout.exitScope();
        return false;
    } else {
        m_elbow->transform(ModelNode::rotateMatrix('x', factor));
        m_forearmFlexFactor += factor;
        dout.exitScope();
        return true;
    }
}

bool Arm::flexHand(double factor) {
    dout.enterScope("Arm::flexHand");

    if (m_handFlexFactor + factor > M_PI / 2) {
        m_wrist->transform(ModelNode::rotateMatrix('x', M_PI / 2 - m_handFlexFactor));
        m_handFlexFactor = M_PI / 2;
        dout.exitScope();
        return false;
    } else if (m_handFlexFactor + factor < M_PI / -2) {
        m_wrist->transform(ModelNode::rotateMatrix('x', M_PI / -2 - m_handFlexFactor));
        m_handFlexFactor = M_PI / -2;
        dout.exitScope();
        return false;
    } else {
        m_wrist->transform(ModelNode::rotateMatrix('x', factor));
        m_handFlexFactor += factor;
        dout.exitScope();
        return true;
    }
}
