/*
 * modelnode.cpp
 *
 * TODO
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#include "modelnode.hpp"
#include "debugstream.hpp"
#include "sphere.hpp"
#include <GL/gl.h>
#include <sstream>
#include <cmath>
#include <iostream>

Matrix4x4 ModelNode::translateMatrix(const Vector4D& v) {
    DebugStream &dout = DebugManager::getInstance().newStream("ModelNode");
    dout.enterScope("ModelNode::translateMatrix");

    Matrix4x4 m;
    m[0][3] = v[0];
    m[1][3] = v[1];
    m[2][3] = v[2];

    dout.exitScope();
    return m;
}

Matrix4x4 ModelNode::scaleMatrix(const Vector4D& v) {
    DebugStream &dout = DebugManager::getInstance().newStream("ModelNode");
    dout.enterScope("ModelNode::scaleMatrix");

    Matrix4x4 m;
    m[0][0] = v[0];
    m[1][1] = v[1];
    m[2][2] = v[2];

    dout.exitScope();
    return m;
}

Matrix4x4 ModelNode::rotateMatrix(char axis, double angle) {
    DebugStream &dout = DebugManager::getInstance().newStream("ModelNode");
    dout.enterScope("ModelNode::rotateMatrix");

    Matrix4x4 m;
    switch (axis) {
      case 'x':
        m[1][1] = cos(angle);
        m[1][2] = -sin(angle);
        m[2][1] = sin(angle);
        m[2][2] = cos(angle);
        break;
      case 'y':
        m[0][0] = cos(angle);
        m[0][2] = sin(angle);
        m[2][0] = -sin(angle);
        m[2][2] = cos(angle);
        break;
      case 'z':
        m[0][0] = cos(angle);
        m[0][1] = -sin(angle);
        m[1][0] = sin(angle);
        m[1][1] = cos(angle);
        break;
    }

    dout.exitScope();
    return m;
}

ModelNode::ModelNode(std::string desc) : dout(DebugManager::getInstance().newStream("ModelNode")) {
    dout.enterScope("ModelNode::ModelNode");

    m_parent = NULL;
    m_desc = desc;

    dout.exitScope();
}

ModelNode::~ModelNode() {
    dout.enterScope("ModelNode::~ModelNode");

    // First, remove self from parent's child list
    if (m_parent != NULL) {
        m_parent->m_childlist.remove(this);
    }

    // Use while here instead of an iterator, because children will remove themselves from this list
    while (!m_childlist.empty()) {
        delete m_childlist.front();
    }

    dout.exitScope();
}

ModelNode* ModelNode::getParent() const {
    dout.enterScope("ModelNode::getParent");

    dout.exitScope();
    return m_parent;
}

ModelNode* ModelNode::getAncestor(std::string desc) const {
    dout.enterScope("ModelNode::getAncestor");

    ModelNode *parent = NULL;

    for (parent = getParent(); parent != NULL; parent = parent->getParent()) {
        if (parent->print(true) == desc) {
            break;
        }
    }

    dout.exitScope();
    return parent;
}

void ModelNode::addChild(ModelNode* n) {
    dout.enterScope("ModelNode::addChild");
    n->m_parent = this;
    m_childlist.push_back(n);
    dout.exitScope();
}

void ModelNode::walk(ModelNode::WalkMode mode) {
    //dout.enterScope("ModelNode::walk");

    walkPush(mode);
    walkBody(mode);
    walkPop(mode);

    //dout.exitScope();
}

void ModelNode::transform(Matrix4x4 m) {
    dout.enterScope("ModelNode::transform");

    dout << "Matrix before transform is " << std::endl << m_transform << std::endl;
    m_transform *= m;
    dout << "Matrix after transform is " << std::endl << m_transform << std::endl;

    dout.exitScope();
}

void ModelNode::printTransform() const {
    dout.enterScope("ModelNode::printTransform");

    std::cout << m_transform << std::endl;

    dout.exitScope();
}

std::string ModelNode::print(bool root) const {
    std::stringstream s;

    if (root) {
        s << "[ModelNode|";
    }

    if (m_desc == "") {
        s << "children=" << m_childlist.size() << "]";
    } else {
        s << m_desc << "]";
    }

    return s.str();
}

void ModelNode::walkBody(WalkMode mode) {
    dout.enterScope("ModelNode::walkBody");

    for (std::list<ModelNode*>::const_iterator it = m_childlist.begin(); it != m_childlist.end(); it++) {
        //dout << "Calling walk on " << **it << std::endl;
        (*it)->walk(mode);
    }

    dout.exitScope();
}

void ModelNode::drawOrigin() const {
    dout.enterScope("ModelNode::drawOrigin");

    Vector4D v = m_transform * Vector4D(0, 0, 0, 1);
    Matrix4x4 m;
    Sphere s;

    m[0][3] = v[0];
    m[1][3] = v[1];
    m[2][3] = v[2];
    m *= ModelNode::scaleMatrix(Vector4D(0.25, 0.25, 0.25, 0));

    glPushMatrix();
    glMultMatrixd(m.transpose().begin());
    s.draw();
    glPopMatrix();

    dout.exitScope();
}

std::ostream& operator<<(std::ostream& os, const ModelNode& n) {
    return os << n.print();
}
