/*
 * gamewindow.cpp
 *
 * This object links the Model, Viewer (Viewport) and
 * Controller (Engine). Its key functionalities are to handle
 * menus, switch between different game modes, and manage
 * memory.
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#include "gamewindow.hpp"
#include "debugstream.hpp"
#include "modelnode.hpp"
#include <iostream>

GameWindow::GameWindow() : m_engine(m_viewport, m_trackball), dout(DebugManager::getInstance().newStream("GameWindow")) {
    dout.enterScope("GameWindow::GameWindow");

    // Set the window title
    set_title("Unreal488");

    // Initialize and add the menus
    initializeMenus();

    // Add the viewport below the menus
    m_vbox.pack_start(m_viewport);

    // Connect the viewport to the virtual trackball
    m_viewport.signal_configure_event().connect(sigc::mem_fun(m_trackball, &Trackball::viewportResized), false);

    // Connect the viewport pick signal to the engine
    m_viewport.pickedSignal().connect(sigc::mem_fun(m_engine, &Engine::onPicked));

    // Reset the model
    m_modelRoot = NULL;

    // Connect the viewport input to this object and reset mouse button state
    m_viewport.signal_button_press_event().connect(sigc::mem_fun(this, &GameWindow::onButtonPress), false);
    m_viewport.signal_button_release_event().connect(sigc::mem_fun(this, &GameWindow::onButtonRelease), false);
    m_viewport.signal_motion_notify_event().connect(sigc::mem_fun(this, &GameWindow::onButtonMotion), false);
    m_viewport.signal_scroll_event().connect(sigc::mem_fun(this, &GameWindow::onMouseScroll), false);
    m_leftPressed = m_rightPressed = m_middlePressed = m_motionLatch = false;
    m_lastX = m_lastY = 0;
    m_shiftPressed = m_tPressed = m_sPressed = m_rPressed = false;

    show_all();

    dout.exitScope();
}

GameWindow::~GameWindow() {
    dout.enterScope("GameWindow::~GameWindow");
    delete m_modelRoot;
    dout.exitScope();
}

bool GameWindow::onButtonPress(GdkEventButton* event) {
    dout.enterScope("GameWindow::onButtonPress");

    switch (event->button) {
      case 1:
        m_leftPressed = true;
        break;
      case 2:
        m_middlePressed = true;
        break;
      case 3:
        m_rightPressed = true;
        break;
      default:
        return false;
    }

    // Create GameInput based on current mouse state
    Engine::GameInput i = getCurrentInput();

    // Update members based on this event
    i.newX = event->x;
    i.newY = event->y;

    // Feed input to engine
    m_engine.input(i);

    dout.exitScope();
    return true;
}

bool GameWindow::onButtonRelease(GdkEventButton* event) {
    dout.enterScope("GameWindow::onButtonRelease");

    switch (event->button) {
      case 1:
        m_leftPressed = false;
        break;
      case 2:
        m_middlePressed = false;
        break;
      case 3:
        m_rightPressed = false;
        break;
      default:
        return false;
    }

    // If there are no buttons being held anymore, reset the motion latch
    if (!m_leftPressed && !m_middlePressed && !m_rightPressed) {
        m_motionLatch = false;
    }

    // Create GameInput based on current mouse state
    Engine::GameInput i = getCurrentInput();

    // Update members based on this event
    i.newX = event->x;
    i.newY = event->y;

    // Feed input to engine
    m_engine.input(i);

    dout.exitScope();
    return true;
}

bool GameWindow::onButtonMotion(GdkEventMotion* event) {
    dout.enterScope("GameWindow::onButtonMotion");

    // To avoid jerky movements, use a "latch" that prevents the first deltaX/Y values from being used
    if (m_motionLatch) {
        // Create GameInput based on current mouse state
        Engine::GameInput i = getCurrentInput();

        // Update members based on this event
        i.newX = event->x;
        i.newY = event->y;
        i.mouseButtons["motion"] = true;

        // Feed input to game engine
        m_engine.input(i);
    } else {
        // If here, the latch is reset. Set it now that we have reasonable lastX/Y values
        m_motionLatch = true;
    }

    // Update motion state
    m_lastX = event->x;
    m_lastY = event->y;

    dout.exitScope();
    return true;
}

bool GameWindow::on_key_press_event(GdkEventKey* event) {
    dout.enterScope("GameWindow::on_key_press_event");

    Engine::GameInput i = getCurrentInput();

    switch (event->keyval) {
      case GDK_KEY_space:
        i.keys["space"] = true;
        m_engine.input(i);
        dout.exitScope();
        return true;
      case GDK_KEY_Shift_L:
      case GDK_KEY_Shift_R:
        m_shiftPressed = true;
        dout.exitScope();
        return true;
      case GDK_KEY_Return:
        i.keys["enter"] = true;
        m_engine.input(i);
        dout.exitScope();
        return true;
      case GDK_KEY_Left:
        i.keys["left"] = true;
        m_engine.input(i);
        dout.exitScope();
        return true;
      case GDK_KEY_Right:
        i.keys["right"] = true;
        m_engine.input(i);
        dout.exitScope();
        return true;
      case GDK_KEY_1:
      case GDK_KEY_KP_1:
        i.keys["1"] = true;
        m_engine.input(i);
        dout.exitScope();
        return true;
      case GDK_KEY_2:
      case GDK_KEY_KP_2:
        i.keys["2"] = true;
        m_engine.input(i);
        dout.exitScope();
        return true;
      case GDK_KEY_3:
      case GDK_KEY_KP_3:
        i.keys["3"] = true;
        m_engine.input(i);
        dout.exitScope();
        return true;
      case GDK_KEY_4:
      case GDK_KEY_KP_4:
        i.keys["4"] = true;
        m_engine.input(i);
        dout.exitScope();
        return true;
      case GDK_KEY_5:
      case GDK_KEY_KP_5:
        i.keys["5"] = true;
        m_engine.input(i);
        dout.exitScope();
        return true;
      case GDK_KEY_T:
      case GDK_KEY_t:
        m_tPressed = true;
        dout.exitScope();
        return true;
      case GDK_KEY_S:
      case GDK_KEY_s:
        m_sPressed = true;
        dout.exitScope();
        return true;
      case GDK_KEY_R:
      case GDK_KEY_r:
        m_rPressed = true;
        dout.exitScope();
        return true;
      case GDK_KEY_Delete:
      case GDK_KEY_KP_Delete:
        i.keys["delete"] = true;
        m_engine.input(i);
        dout.exitScope();
        return true;
      default:
        break;
    }

    Gtk::Window::on_key_press_event(event);
    dout.exitScope();
    return false;
}

bool GameWindow::on_key_release_event(GdkEventKey* event) {
    dout.enterScope("GameWindow::on_key_release_event");

    switch (event->keyval) {
      case GDK_KEY_Shift_L:
      case GDK_KEY_Shift_R:
        m_shiftPressed = false;
        dout.exitScope();
        return true;
      case GDK_KEY_T:
      case GDK_KEY_t:
        m_tPressed = false;
        dout.exitScope();
        return true;
      case GDK_KEY_S:
      case GDK_KEY_s:
        m_sPressed = false;
        dout.exitScope();
        return true;
      case GDK_KEY_R:
      case GDK_KEY_r:
        m_rPressed = false;
        dout.exitScope();
        return true;
    }

    Gtk::Window::on_key_release_event(event);
    dout.exitScope();
    return false;
}

bool GameWindow::onMouseScroll(GdkEventScroll* event) {
    dout.enterScope("GameWindow::onMouseScroll");

    Engine::GameInput i = getCurrentInput();

    switch (event->direction) {
      case GDK_SCROLL_UP:
        i.mouseButtons["scrollUp"] = true;
        m_engine.input(i);
        dout.exitScope();
        return true;
      case GDK_SCROLL_DOWN:
        i.mouseButtons["scrollDown"] = true;
        m_engine.input(i);
        dout.exitScope();
        return true;
      default:
        break;
    }

    dout.exitScope();
    return false;
}

void GameWindow::initializeMenus() {
    dout.enterScope("GameWindow::initializeMenus");

    // Menu element utility classes
    using Gtk::Menu_Helpers::MenuElem;

    // Set up the application menu
    sigc::slot1<void, Engine::GameType> game_slot = sigc::mem_fun(*this, &GameWindow::changeGame);

    m_appMenu.items().push_back(MenuElem("Start Designing", sigc::bind(game_slot, Engine::DESIGN)));
    m_appMenu.items().push_back(MenuElem("_Quit", Gtk::AccelKey("q"), sigc::mem_fun(*this, &GameWindow::hide)));

    // Add the individual menus to the menu bar
    m_menuBar.items().push_back(Gtk::Menu_Helpers::MenuElem("_Application", m_appMenu));

    // Add the vertical box as the single "top" widget
    add(m_vbox);

    // Put the menubar on the top, and make it as small as possible
    m_vbox.pack_start(m_menuBar, Gtk::PACK_SHRINK);

    dout.exitScope();
}

void GameWindow::initializeDesignMenus() {
    dout.enterScope("GameWindow::initializeDesignMenus");

    // Menu element utility classes
    using Gtk::Menu_Helpers::MenuElem;
    using Gtk::Menu_Helpers::CheckMenuElem;

    // Set up the options menu
    m_optionMenu.items().push_back(CheckMenuElem("_Backface cull", Gtk::AccelKey("b"), sigc::mem_fun(m_viewport, &Viewport::toggleBackCull)));
    m_optionMenu.items().push_back(CheckMenuElem("_Frontface cull", Gtk::AccelKey("f"), sigc::mem_fun(m_viewport, &Viewport::toggleFrontCull)));
    m_optionMenu.items().push_back(CheckMenuElem("_Wireframe", Gtk::AccelKey("w"), sigc::mem_fun(m_viewport, &Viewport::toggleWireFrame)));

    // Add the individual menus to the menu bar
    m_menuBar.items().push_back(Gtk::Menu_Helpers::MenuElem("_Options", m_optionMenu));

    dout.exitScope();
}

void GameWindow::changeGame(Engine::GameType t) {
    dout.enterScope("GameWindow::changeGame");

    // Make this idempotent
    if (t == m_engine.getCurrentType()) {
        return;
    }

    ModelNode *newRoot;

    switch (t) {
      case Engine::DESIGN:
        initializeDesignMenus();
        newRoot = m_engine.init(t);
        break;
      case Engine::NONE:
      default:
        throw EngineException("Can't call GameWindow::changeGame with Engine::NONE!");
    }

    // If we already had a model tree, delete it
    if (m_modelRoot != NULL) {
        delete m_modelRoot;
    }

    // Initialize GL code in new model
    if (newRoot != NULL) {
        newRoot->walk(ModelNode::INIT_GL);
    }

    // Adopt new model and attach to the viewer
    m_modelRoot = newRoot;
    m_viewport.attachModel(newRoot);

    dout.exitScope();
}

Engine::GameInput GameWindow::getCurrentInput() const {
    dout.enterScope("GameWindow::getCurrentInput");

    Engine::GameInput i;
    i.oldX = m_lastX;
    i.oldY = m_lastY;
    i.mouseButtons["left"] = m_leftPressed;
    i.mouseButtons["middle"] = m_middlePressed;
    i.mouseButtons["right"] = m_rightPressed;
    i.keys["shift"] = m_shiftPressed;
    i.keys["t"] = m_tPressed;
    i.keys["s"] = m_sPressed;
    i.keys["r"] = m_rPressed;

    dout.exitScope();
    return i;
}
