/*
 * world.cpp
 *
 * TODO
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#include "world.hpp"
#include "debugstream.hpp"
#include "player.hpp"

World::World() : dout(DebugManager::getInstance().newStream("World")) {
    dout.enterScope("World::World");
    dout.exitScope();
}

World::~World() {
    dout.enterScope("World::~World");
    dout.exitScope();
}

void World::addPlayer(Player* p) {
    dout.enterScope("World::addPlayer");

    // Add to model data structure
    ModelNode::addChild(p);

    // Add to player shadow list
    m_playerList.insert(p);

    dout.exitScope();
}
