/*
 * virtualnode.cpp
 *
 * TODO
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#include "virtualnode.hpp"
#include "debugstream.hpp"
#include "sphere.hpp"
#include "colour.hpp"
#include <GL/gl.h>
#include <sstream>

VirtualNode::VirtualNode(std::string desc) : ModelNode(desc), dout(DebugManager::getInstance().newStream("VirtualNode")) {
    dout.enterScope("VirtualNode::VirtualNode");
    dout.exitScope();
}

VirtualNode::~VirtualNode() {
    dout.enterScope("VirtualNode::~VirtualNode");
    dout.exitScope();
}

void VirtualNode::draw() const {
    dout.enterScope("VirtualNode::draw");

    Colour blue = Colour(Colour::BLUE);
    Colour white = Colour(Colour::WHITE);

    glColor3d(blue.R(), blue.G(), blue.B());
    ModelNode::drawOrigin();
    glColor3d(white.R(), white.G(), white.B());

    dout.exitScope();
}


std::string VirtualNode::print(bool root) const {
    std::stringstream s;

    if (root) {
        s << "[VirtualNode|";
    }

    s << ModelNode::print(false);

    return s.str();
}

void VirtualNode::walkPush(ModelNode::WalkMode mode) {
    // Draw reference point before transforms
    if (mode == ModelNode::DRAW_DEBUG) {
        draw();
    }

    // Push and apply transforms before traversing children
    if (mode == ModelNode::DRAW || mode == ModelNode::DRAW_DEBUG || mode == ModelNode::PICK) {
        glPushMatrix();
        glMultMatrixd(m_transform.transpose().begin());
    }
}

void VirtualNode::walkBody(ModelNode::WalkMode mode) {
    dout.enterScope("VirtualNode::walkBody");

    // Call parent walk to reach more children
    ModelNode::walkBody(mode);

    dout.exitScope();
}

void VirtualNode::walkPop(ModelNode::WalkMode mode) {
    // Call pop after the children run their draws
    if (mode == ModelNode::DRAW || mode == ModelNode::DRAW_DEBUG || mode == ModelNode::PICK) {
        glPopMatrix();
    }
}

std::ostream& operator<<(std::ostream& os, const VirtualNode& n) {
    return os << n.print();
}
