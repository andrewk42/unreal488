/*
 * sphere.hpp
 *
 * The Sphere primitive.
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#ifndef SPHERE_HPP
#define SPHERE_HPP

#include "primitive.hpp"
#include <GL/gl.h>

class Sphere : public Primitive {
  public:
    Sphere(unsigned radius = 1, unsigned slices = 10, unsigned stacks = 20);
    virtual ~Sphere();

    virtual void initGL();
    virtual void draw() const;
    virtual std::string print() const;

  private:
    // Members
    // OpenGL Display list index
    static GLuint m_displayListIndex;
    // Drawing parameters
    unsigned m_radius, m_slices, m_stacks;
    // Debug output
    DebugStream& dout;
};

std::ostream& operator<<(std::ostream& os, const Sphere& s);

#endif // SPHERE_HPP
