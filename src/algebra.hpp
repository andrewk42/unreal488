/*
 * algebra.hpp
 *
 * Objects that are useful for performing various algebraic
 * operations. They are represented as a 4D affine space, to
 * allow for typical affine transforms.
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#ifndef ALGEBRA_HPP
#define ALGEBRA_HPP

#include <stdexcept>
#include <ostream>

class DebugStream;

class AlgebraException : public std::runtime_error {
  public:
    AlgebraException(std::string msg = "AlgebraException");
};

class Vector4D {
  public:
    Vector4D(double x = 0, double y = 0, double z = 0, double w = 0);
    Vector4D(const Vector4D& other);
    Vector4D& operator=(const Vector4D& other);

    double& operator[](unsigned index);
    double operator[](unsigned index) const;

    Vector4D cross(const Vector4D& other) const;
    double length() const;
    Vector4D normalize() const;

  private:
    // Friends
    friend std::ostream& operator<<(std::ostream& os, const Vector4D& v);

    // Members
    // Element data
    double m_data[4];
    // Debug output
    DebugStream& dout;
};

class Matrix4x4 {
  public:
    Matrix4x4();
    Matrix4x4(const Vector4D row1, const Vector4D row2, const Vector4D row3, const Vector4D row4);
    Matrix4x4(const Matrix4x4& other);
    Matrix4x4& operator=(const Matrix4x4& other);

    Vector4D getRow(unsigned row) const;
    double * getRow(unsigned row);

    Vector4D getColumn(unsigned col) const;
    Vector4D operator[](unsigned row) const;
    double * operator[](unsigned row);
    Matrix4x4 transpose() const;
    Matrix4x4 invert() const;

    const double * begin() const;
    const double * end() const;

    Vector4D operator*(const Vector4D& v) const;
    void operator*=(const Matrix4x4& other);

  private:
    // Friends
    friend Matrix4x4 operator*(const Matrix4x4& a, const Matrix4x4& b);
    friend std::ostream& operator<<(std::ostream& os, const Matrix4x4& m);

    // Members
    // Element data
    double m_data[16];
    // Debug output
    DebugStream& dout;
};

#endif // ALGEBRA_HPP
