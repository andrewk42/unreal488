/*
 * engine.cpp
 *
 * TODO
 *
 * Written for CS488 Fall 2013 for University of Waterloo
 * by Andrew Klamut (ajklamut)
 * #20281315
 */

#include "engine.hpp"
#include "world.hpp"
#include "viewport.hpp"
#include "trackball.hpp"
#include "debugstream.hpp"
#include "modelnode.hpp"
#include "realnode.hpp"
#include "virtualnode.hpp"
#include "colour.hpp"
#include "human.hpp"
#include "leg.hpp"
#include "arm.hpp"
#include "sphere.hpp"
#include "cube.hpp"
#include <cmath>
#include <iostream>

/*
 * EngineException definitions
 */

EngineException::EngineException(std::string msg) : std::runtime_error(msg) {

}


/*
 * Engine::GameInput definitions
 */

Engine::GameInput::GameInput() {
    newX = oldX = newY = oldY = 0;
    mouseButtons.insert(std::pair<std::string, bool>("left", false));
    mouseButtons.insert(std::pair<std::string, bool>("middle", false));
    mouseButtons.insert(std::pair<std::string, bool>("right", false));
    mouseButtons.insert(std::pair<std::string, bool>("motion", false));
    mouseButtons.insert(std::pair<std::string, bool>("scrollUp", false));
    mouseButtons.insert(std::pair<std::string, bool>("scrollDown", false));

    keys.insert(std::pair<std::string, bool>("shift", false));
    keys.insert(std::pair<std::string, bool>("space", false));
    keys.insert(std::pair<std::string, bool>("enter", false));
    keys.insert(std::pair<std::string, bool>("left", false));
    keys.insert(std::pair<std::string, bool>("right", false));
    keys.insert(std::pair<std::string, bool>("1", false));
    keys.insert(std::pair<std::string, bool>("2", false));
    keys.insert(std::pair<std::string, bool>("3", false));
    keys.insert(std::pair<std::string, bool>("4", false));
    keys.insert(std::pair<std::string, bool>("5", false));
    keys.insert(std::pair<std::string, bool>("t", false));
    keys.insert(std::pair<std::string, bool>("s", false));
    keys.insert(std::pair<std::string, bool>("r", false));
    keys.insert(std::pair<std::string, bool>("delete", false));
}


/*
 * Engine definitions
 */

Engine::Engine(Viewport& v, Trackball& t) : m_viewport(v), m_trackball(t), dout(DebugManager::getInstance().newStream("Engine")) {
    dout.enterScope("Engine::Engine");
    m_gameType = Engine::NONE;
    m_modelRoot = NULL;
    m_currentPick = NULL;
    m_complexPick = NULL;
    dout.exitScope();
}

Engine::~Engine() {
    dout.enterScope("Engine::~Engine");
    dout.exitScope();
}

ModelNode* Engine::init(Engine::GameType t) {
    dout.enterScope("Engine::init");

    switch (t) {
      case Engine::DESIGN:
        setupDesigner();
        m_gameType = Engine::DESIGN;
        break;
      case Engine::NONE:
      default:
        throw EngineException("Can't call Engine::init with GameType::NONE!");
    }

    dout.exitScope();
    return m_modelRoot;
}

Engine::GameType Engine::getCurrentType() const {
    return m_gameType;
}

void Engine::input(GameInput i) {
    dout.enterScope("Engine::input");

    switch (m_gameType) {
      case Engine::DESIGN:
        inputDesigner(i);
        break;
      case Engine::PLAY:
      case Engine::NONE:
      default:
        break;
    }

    dout.exitScope();
}

void Engine::onPicked(Viewport::PickResponse response) {
    dout.enterScope("Engine::onPicked");

    switch (m_gameType) {
      case Engine::DESIGN:
        pickDesigner(response);
        break;
      case Engine::PLAY:
      case Engine::NONE:
      default:
        break;
    }

    dout.exitScope();
}

void Engine::setupDesigner() {
    dout.enterScope("Engine::setupDesigner");

    // Create world
    World *w = new World();

    // Initialize primitives
    Sphere s;
    s.initGL();

    Cube c;
    c.initGL();

    // Make the world the root of the model tree
    m_modelRoot = w;

    dout.exitScope();
}

void Engine::inputDesigner(GameInput i) {
    dout.enterScope("Engine::inputDesigner");

    // If motion
    if (i.mouseButtons["motion"]) {
        // Not mutually exclusive
        if (i.mouseButtons["left"]) {
            if (m_currentPick == NULL) {
                if (m_complexPick != NULL) {
                    // For hygiene
                    throw EngineException("Misaligned complex pick pointer");
                }

                translateWorldXY(i.newX - i.oldX, i.newY - i.oldY);
            } else {
                if (i.keys["t"]) {
                    transformModelX(Engine::TRANSLATE, i.newX - i.oldX);
                } else if (i.keys["s"]) {
                    transformModelX(Engine::SCALE, i.newX - i.oldX);
                } else if (i.keys["r"]) {
                    transformModelX(Engine::ROTATE, i.newX - i.oldX);
                }
            }
        }

        if (i.mouseButtons["middle"]) {
            if (m_currentPick == NULL) {
                if (m_complexPick != NULL) {
                    // For hygiene
                    throw EngineException("Misaligned complex pick pointer");
                }

                translateWorldZ(i.newY - i.oldY);
            } else {
                if (i.keys["t"]) {
                    transformModelZ(Engine::TRANSLATE, i.newY - i.oldY);
                } else if (i.keys["s"]) {
                    transformModelZ(Engine::SCALE, i.newY - i.oldY);
                } else if (i.keys["r"]) {
                    transformModelZ(Engine::ROTATE, i.newY - i.oldY);
                }
            }
        }

        if (i.mouseButtons["right"]) {
            if (m_currentPick == NULL) {
                if (m_complexPick != NULL) {
                    // For hygiene
                    throw EngineException("Misaligned complex pick pointer");
                }

                rotateWorld(i.newX, i.newY, i.oldX, i.oldY);
            } else {
                if (i.keys["t"]) {
                    transformModelY(Engine::TRANSLATE, i.newY - i.oldY);
                } else if (i.keys["s"]) {
                    transformModelY(Engine::SCALE, i.newY - i.oldY);
                } else if (i.keys["r"]) {
                    transformModelY(Engine::ROTATE, i.newY - i.oldY);
                }
            }
        }
    } else {
        if (i.keys["space"]) {
            if (m_complexPick != NULL) {
                std::cout << "Transform matrix for " << *m_complexPick << ":" << std::endl;
                m_complexPick->printTransform();
            } else {
                if (m_currentPick != NULL) {
                    std::cout << "Transform matrix for " << *m_currentPick << ":" << std::endl;
                    m_currentPick->printTransform();
                }
            }
        } else if (i.keys["1"]) {
            m_modelRoot->addChild(new Human("Human"));
        } else if (i.keys["2"]) {
            m_modelRoot->addChild(new Arm("LeftArm"));
        } else if (i.keys["3"]) {
            m_modelRoot->addChild(new Leg("RightLeg"));
        } else if (i.keys["4"]) {
            m_modelRoot->addChild(new RealNode(new Sphere()));
        } else if (i.keys["5"]) {
            m_modelRoot->addChild(new RealNode(new Cube()));
        } else if (!(i.keys["t"] || i.keys["s"] || i.keys["r"] || i.keys["shift"]) && i.mouseButtons["left"]) {
            m_viewport.pick(i.newX, i.newY, 3, 3);
        } else if (i.mouseButtons["scrollUp"]) {
            animateModel(true);
        } else if (i.mouseButtons["scrollDown"]) {
            animateModel(false);
        } else if (i.keys["delete"]) {
            if (m_complexPick != NULL) {
                delete m_complexPick;
                m_complexPick = NULL;
                m_currentPick = NULL;
            } else if (m_currentPick != NULL) {
                delete m_currentPick;
                m_currentPick = NULL;
            }
        }
    }

    dout.exitScope();
}

void Engine::translateWorldXY(int deltaX, int deltaY) {
    dout.enterScope("Engine::translateWorldXY");

    // Express desired transform as RHS matrix
    Matrix4x4 m;

    // For left motion, translate model in xy
    m[0][3] += (double)deltaX / 100;
    m[1][3] -= (double)deltaY / 100;

    m_modelRoot->transform(m);

    dout.exitScope();
}

void Engine::translateWorldZ(int deltaY) {
    dout.enterScope("Engine::translateWorldZ");

    // Express desired transform as RHS matrix
    Matrix4x4 m;

    // For middle motion, translate model along z
    m[2][3] += (double)deltaY / 50;

    m_modelRoot->transform(m);

    dout.exitScope();
}

void Engine::rotateWorld(int newX, int newY, int oldX, int oldY) {
    dout.enterScope("Engine::rotateWorld");

    Matrix4x4 m = m_trackball.getTransform(newX, newY, oldX, oldY);
    m_modelRoot->transform(m);

    dout.exitScope();
}

void Engine::transformModelX(Engine::TransformMode m, int deltaX) {
    dout.enterScope("Engine::transformModelX");

    switch (m) {
      case Engine::TRANSLATE:
        if (m_complexPick != NULL) {
            m_complexPick->transform(ModelNode::translateMatrix(Vector4D((double)deltaX / 100, 0, 0, 0)));
        } else {
            if (m_currentPick == NULL) {
                // For hygiene
                throw EngineException("Misaligned current pick pointer");
            }

            m_currentPick->transform(ModelNode::translateMatrix(Vector4D((double)deltaX / 100, 0, 0, 0)));
        }
        break;
      case Engine::SCALE:
        if (m_complexPick != NULL) {
            m_complexPick->transform(ModelNode::scaleMatrix(Vector4D(1 + (double)deltaX / 1000, 1, 1, 0)));
        } else {
            if (m_currentPick == NULL) {
                // For hygiene
                throw EngineException("Misaligned current pick pointer");
            }

            m_currentPick->transform(ModelNode::scaleMatrix(Vector4D(1 + (double)deltaX / 1000, 1, 1, 0)));
        }
        break;
      case Engine::ROTATE:
        if (m_complexPick != NULL) {
            m_complexPick->transform(ModelNode::rotateMatrix('x', (double)deltaX / 10));
        } else {
            if (m_currentPick == NULL) {
                // For hygiene
                throw EngineException("Misaligned current pick pointer");
            }

            m_currentPick->transform(ModelNode::rotateMatrix('x', (double)deltaX / 10));
        }
        break;
      default:
        break;
    }

    dout.exitScope();
}

void Engine::transformModelY(Engine::TransformMode m, int deltaY) {
    dout.enterScope("Engine::transformModelY");

    switch (m) {
      case Engine::TRANSLATE:
        if (m_complexPick != NULL) {
            m_complexPick->transform(ModelNode::translateMatrix(Vector4D(0, (double)deltaY / -100, 0, 0)));
        } else {
            if (m_currentPick == NULL) {
                // For hygiene
                throw EngineException("Misaligned current pick pointer");
            }

            m_currentPick->transform(ModelNode::translateMatrix(Vector4D(0, (double)deltaY / -100, 0, 0)));
        }
        break;
      case Engine::SCALE:
        if (m_complexPick != NULL) {
            m_complexPick->transform(ModelNode::scaleMatrix(Vector4D(1, 1 + (double)deltaY / 1000, 1, 0)));
        } else {
            if (m_currentPick == NULL) {
                // For hygiene
                throw EngineException("Misaligned current pick pointer");
            }

            m_currentPick->transform(ModelNode::scaleMatrix(Vector4D(1, 1 + (double)deltaY / 1000, 1, 0)));
        }
        break;
      case Engine::ROTATE:
        if (m_complexPick != NULL) {
            m_complexPick->transform(ModelNode::rotateMatrix('y', (double)deltaY / 10));
        } else {
            if (m_currentPick == NULL) {
                // For hygiene
                throw EngineException("Misaligned current pick pointer");
            }

            m_currentPick->transform(ModelNode::rotateMatrix('y', (double)deltaY / 10));
        }
        break;
      default:
        break;
    }

    dout.exitScope();
}

void Engine::transformModelZ(Engine::TransformMode m, int deltaY) {
    dout.enterScope("Engine::transformModelZ");

    switch (m) {
      case Engine::TRANSLATE:
        if (m_complexPick != NULL) {
            m_complexPick->transform(ModelNode::translateMatrix(Vector4D(0, 0, (double)deltaY / 50, 0)));
        } else {
            if (m_currentPick == NULL) {
                // For hygiene
                throw EngineException("Misaligned current pick pointer");
            }

            m_currentPick->transform(ModelNode::translateMatrix(Vector4D(0, 0, (double)deltaY / 50, 0)));
        }
        break;
      case Engine::SCALE:
        if (m_complexPick != NULL) {
            m_complexPick->transform(ModelNode::scaleMatrix(Vector4D(1, 1, 1 + (double)deltaY / 1000, 0)));
        } else {
            if (m_currentPick == NULL) {
                // For hygiene
                throw EngineException("Misaligned current pick pointer");
            }

            m_currentPick->transform(ModelNode::scaleMatrix(Vector4D(1, 1, 1 + (double)deltaY / 1000, 0)));
        }
        break;
      case Engine::ROTATE:
        if (m_complexPick != NULL) {
            m_complexPick->transform(ModelNode::rotateMatrix('z', (double)deltaY / 10));
        } else {
            if (m_currentPick == NULL) {
                // For hygiene
                throw EngineException("Misaligned current pick pointer");
            }

            m_currentPick->transform(ModelNode::rotateMatrix('z', (double)deltaY / 10));
        }
        break;
      default:
        break;
    }

    dout.exitScope();
}

void Engine::pickDesigner(Viewport::PickResponse response) {
    dout.enterScope("Engine::pickedDesigner");

    RealNode *picked = NULL;
    ModelNode *parent = NULL;
    std::list<ModelNode*> parents;

    // Clear everything if empty list
    if (response.models.empty()) {
        if (m_currentPick != NULL) {
            m_currentPick->select(false);
            m_currentPick = NULL;
        }

        if (m_complexPick != NULL) {
            m_complexPick->walk(ModelNode::DESELECT);
            m_complexPick = NULL;
        }

        return;
    }

    for (std::list<RealNode*>::iterator it = response.models.begin(); it != response.models.end(); it++) {
        picked = *it;

        if (picked == NULL) {
            throw EngineException("Got null pointer in pick response");
        }

        // If nothing picked or this is a new basic pick, pick the first thing
        if (m_currentPick == NULL || m_currentPick != picked) {
            // Do any deselections first
            if (m_currentPick != NULL) {
                m_currentPick->select(false);

                if (m_complexPick != NULL) {
                    m_complexPick->walk(ModelNode::DESELECT);
                    m_complexPick = NULL;
                }
            }

            picked->select();
            m_currentPick = picked;
            std::cout << "Basic pick on " << *picked << std::endl;

            dout.exitScope();
            return;
        // If we are re-picking an already selected item
        } else {
            // If no complex pick, complex pick the first parent
            if (m_complexPick == NULL) {
                parent = picked->getParent();
                m_complexPick = dynamic_cast<VirtualNode*>(parent);

                if (m_complexPick == NULL) {
                    throw EngineException("Got non-virtual parent of currently picked node");
                } else {
                    m_complexPick->walk(ModelNode::SELECT);
                    std::cout << "Complex pick on " << *m_complexPick << std::endl;
                    dout.exitScope();
                    return;
                }
            // If already complex picked, walk backwards to find the currently complex-picked parent
            } else {
                for (parent = picked->getParent(); parent != NULL; parent = parent->getParent()) {
                    // If we found the currently complex-picked parent
                    if (parent == m_complexPick) {
                        // If it's the world, deselect and let move on to next picked
                        if (parent == m_modelRoot) {
                            m_complexPick->walk(ModelNode::DESELECT);
                            m_complexPick = NULL;
                            break;
                        // Otherwise, select its parent, and return
                        } else {
                            m_complexPick = dynamic_cast<VirtualNode*>(parent->getParent());

                            if (m_complexPick == NULL) {
                                throw EngineException("Got non-virtual parent of current complex pick");
                            } else {
                                m_complexPick->walk(ModelNode::SELECT);
                                std::cout << "Complex pick on " << *m_complexPick << std::endl;
                                dout.exitScope();
                                return;
                            }
                        }
                    }
                }

                if (m_complexPick != NULL) {
                    // Should never get here unless the complex pick pointer is dirty
                    throw EngineException("Misaligned complex pick pointer");
                }
            }
        }
    }

    dout.exitScope();
}

void Engine::animateModel(bool reverse) {
    dout.enterScope("Engine::animateModel");

    if (m_currentPick == NULL) {
        return;
    }

    std::string desc;
    ModelNode *selected = NULL;
    double magnitude = M_PI / -12;

    if (m_complexPick == NULL) {
        desc = m_currentPick->print(true);
        selected = m_currentPick;
    } else {
        desc = m_complexPick->print(true);
        selected = m_complexPick;
    }

    if (reverse) {
        magnitude *= -1;
    }

    if (desc == "[RealNode|LeftLeg|foot]" || desc == "[VirtualNode|LeftLeg|ankle]") {
        Leg *l = dynamic_cast<Leg*>(selected->getAncestor("[VirtualNode|LeftLeg]"));

        if (l == NULL) {
            throw EngineException("Couldn't find ancestor for animation");
        }

        if (!l->flexFoot(magnitude)) {
            std::cout << *selected << "too far!" << std::endl;
        }
    } else if (desc == "[RealNode|RightLeg|foot]" || desc == "[VirtualNode|RightLeg|ankle]") {
        Leg *l = dynamic_cast<Leg*>(selected->getAncestor("[VirtualNode|RightLeg]"));

        if (l == NULL) {
            throw EngineException("Couldn't find ancestor for animation");
        }

        if (!l->flexFoot(magnitude)) {
            std::cout << *selected << "too far!" << std::endl;
        }
    } else if (desc == "[RealNode|LeftLeg|calf]" || desc == "[VirtualNode|LeftLeg|knee]") {
        Leg *l = dynamic_cast<Leg*>(selected->getAncestor("[VirtualNode|LeftLeg]"));

        if (l == NULL) {
            throw EngineException("Couldn't find ancestor for animation");
        }

        if (!l->flexCalf(magnitude)) {
            std::cout << *selected << "too far!" << std::endl;
        }
    } else if (desc == "[RealNode|RightLeg|calf]" || desc == "[VirtualNode|RightLeg|knee]") {
        Leg *l = dynamic_cast<Leg*>(selected->getAncestor("[VirtualNode|RightLeg]"));

        if (l == NULL) {
            throw EngineException("Couldn't find ancestor for animation");
        }

        if (!l->flexCalf(magnitude)) {
            std::cout << *selected << "too far!" << std::endl;
        }
    } else if (desc == "[RealNode|LeftLeg|thigh]" || desc == "[VirtualNode|LeftLeg]") {
        Leg *l = NULL;

        if (desc == "[VirtualNode|LeftLeg]") {
            l = dynamic_cast<Leg*>(selected);
        } else {
            l = dynamic_cast<Leg*>(selected->getAncestor("[VirtualNode|LeftLeg]"));
        }

        if (l == NULL) {
            throw EngineException("Couldn't find ancestor for animation");
        }

        if (!l->flexThigh(magnitude)) {
            std::cout << *selected << "too far!" << std::endl;
        }
    } else if (desc == "[RealNode|RightLeg|thigh]" || desc == "[VirtualNode|RightLeg]") {
        Leg *l = NULL;

        if (desc == "[VirtualNode|RightLeg]") {
            l = dynamic_cast<Leg*>(selected);
        } else {
            l = dynamic_cast<Leg*>(selected->getAncestor("[VirtualNode|RightLeg]"));
        }

        if (l == NULL) {
            throw EngineException("Couldn't find ancestor for animation");
        }

        if (!l->flexThigh(magnitude)) {
            std::cout << *selected << "too far!" << std::endl;
        }
    } else if (desc == "[RealNode|LeftArm|hand]" || desc == "[VirtualNode|LeftArm|wrist]") {
        Arm *a = dynamic_cast<Arm*>(selected->getAncestor("[VirtualNode|LeftArm]"));

        if (a == NULL) {
            throw EngineException("Couldn't find ancestor for animation");
        }

        if (!a->flexHand(magnitude)) {
            std::cout << *selected << "too far!" << std::endl;
        }
    } else if (desc == "[RealNode|RightArm|hand]" || desc == "[VirtualNode|RightArm|wrist]") {
        Arm *a = dynamic_cast<Arm*>(selected->getAncestor("[VirtualNode|RightArm]"));

        if (a == NULL) {
            throw EngineException("Couldn't find ancestor for animation");
        }

        if (!a->flexHand(magnitude)) {
            std::cout << *selected << "too far!" << std::endl;
        }
    } else if (desc == "[RealNode|LeftArm|forearm]" || desc == "[VirtualNode|LeftArm|elbow]") {
        Arm *a = dynamic_cast<Arm*>(selected->getAncestor("[VirtualNode|LeftArm]"));

        if (a == NULL) {
            throw EngineException("Couldn't find ancestor for animation");
        }

        if (!a->flexForearm(magnitude)) {
            std::cout << *selected << "too far!" << std::endl;
        }
    } else if (desc == "[RealNode|RightArm|forearm]" || desc == "[VirtualNode|RightArm|elbow]") {
        Arm *a = dynamic_cast<Arm*>(selected->getAncestor("[VirtualNode|RightArm]"));

        if (a == NULL) {
            throw EngineException("Couldn't find ancestor for animation");
        }

        if (!a->flexForearm(magnitude)) {
            std::cout << *selected << "too far!" << std::endl;
        }
    } else if (desc == "[RealNode|LeftArm|upperarm]" || desc == "[VirtualNode|LeftArm]") {
        Arm *a = NULL;

        if (desc == "[VirtualNode|LeftArm]") {
            a = dynamic_cast<Arm*>(selected);
        } else {
            a = dynamic_cast<Arm*>(selected->getAncestor("[VirtualNode|LeftArm]"));
        }

        if (a == NULL) {
            throw EngineException("Couldn't find ancestor for animation");
        }

        if (!a->flexUpperarm(magnitude)) {
            std::cout << *selected << "too far!" << std::endl;
        }
    } else if (desc == "[RealNode|RightArm|upperarm]" || desc == "[VirtualNode|RightArm]") {
        Arm *a = NULL;

        if (desc == "[VirtualNode|RightArm]") {
            a = dynamic_cast<Arm*>(selected);
        } else {
            a = dynamic_cast<Arm*>(selected->getAncestor("[VirtualNode|RightArm]"));
        }

        if (a == NULL) {
            throw EngineException("Couldn't find ancestor for animation");
        }

        if (!a->flexUpperarm(magnitude)) {
            std::cout << *selected << "too far!" << std::endl;
        }
    } else if (desc == "[RealNode|hips]" || desc == "[VirtualNode|waist]") {
        Human *h = dynamic_cast<Human*>(selected->getAncestor("[VirtualNode|Human]"));

        if (h == NULL) {
            throw EngineException("Couldn't find ancestor for animation");
        }

        if (!h->twistHips(magnitude)) {
            std::cout << *selected << "too far!" << std::endl;
        }
    } else if (desc == "[RealNode|shoulders]" || desc == "[VirtualNode|collar]") {
        Human *h = dynamic_cast<Human*>(selected->getAncestor("[VirtualNode|Human]"));

        if (h == NULL) {
            throw EngineException("Couldn't find ancestor for animation");
        }

        if (!h->twistShoulders(magnitude)) {
            std::cout << *selected << "too far!" << std::endl;
        }
    } else if (desc == "[RealNode|neck]") {
        Human *h = dynamic_cast<Human*>(selected->getAncestor("[VirtualNode|Human]"));

        if (h == NULL) {
            throw EngineException("Couldn't find ancestor for animation");
        }

        if (!h->flexNeck(magnitude)) {
            std::cout << *selected << "too far!" << std::endl;
        }
    } else if (desc == "[RealNode|head]") {
        Human *h = dynamic_cast<Human*>(selected->getAncestor("[VirtualNode|Human]"));

        if (h == NULL) {
            throw EngineException("Couldn't find ancestor for animation");
        }

        if (!h->twistNeck(magnitude)) {
            std::cout << *selected << "too far!" << std::endl;
        }
    }

    dout.exitScope();
}
